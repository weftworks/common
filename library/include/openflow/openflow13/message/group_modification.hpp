// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include "openflow/common.hpp"
#include "openflow/group.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {

namespace implementation {

/// present buckets for groups
///
struct bucket
{
        /// constructor
        ///
        bucket();

        uint16_t length;        ///< bucket length in bytes
        uint16_t weight;        ///< relative bucket weight
        uint32_t watch_port;    ///< port which affects this bucket
        uint32_t watch_group;   ///< group which affects this bucket
        uint32_t padding;       ///< data padding
        // TODO action
};

/// present a group modification message
///
struct group_modification
{
        /// constructor
        ///
        group_modification();

        openflow::header header;        ///< openflow header
        uint16_t command;               ///< group modification command
        uint8_t type;                   ///< group type
        uint8_t padding;                ///< data padding
        uint32_t group_id;              ///< group identifier
        std::vector<bucket> buckets;    ///< buckets
};

} // namespace implementation

// lift the implementation
using group_modification = implementation::group_modification;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const group_modification& message) -> std::string;

} // namespace weftworks::common::openflow::openflow13::message
