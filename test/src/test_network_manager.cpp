// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <catch2/catch.hpp>

#include "network.hpp"

/// test the connection class
///
class test_connection
        : public weftworks::common::network::tcp::connection<test_connection>
{
public:
        /// constructor
        ///
        using weftworks::common::network::tcp::connection<test_connection>::connection;
};

TEST_CASE("network manager", "[require]")
{
        auto io_context = boost::asio::io_context{};
        auto network_manager = std::make_shared<weftworks::common::network::tcp::network_manager<test_connection>>(io_context);

        SECTION("should be correctly deleted")
        {
                auto weak_ptr = std::weak_ptr<weftworks::common::network::tcp::network_manager<test_connection>>{network_manager};
                REQUIRE(weak_ptr.lock());
                network_manager.reset();
                REQUIRE(not weak_ptr.lock());
        }
}
