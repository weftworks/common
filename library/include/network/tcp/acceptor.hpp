// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <optional>
#include <string_view>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/placeholders.hpp>
#include <spdlog/spdlog.h>

#include "utility.hpp"

#include "connection_pool.hpp"

namespace weftworks::common::network::tcp {
namespace implementation {

/// accept tcp connections
///
template<class T, template<class...> typename C = std::shared_ptr>
class acceptor
        : public utility::noncopyable
{
public:
        /// constructor
        ///
        /// \param io_context is a reference to a boost asio io context
        acceptor(boost::asio::io_context& io_context);

        /// move constructor
        ///
        acceptor(acceptor&&);

        /// deconstructor
        ///
        ~acceptor();

        /// move assignment operator
        ///
        auto operator=(acceptor&&) -> acceptor&;

        /// get the port number currently being used by the acceptor
        ///
        /// returns 0 when the acceptor is not bound
        /// \return a port number
        auto get_port() const noexcept -> uint16_t;

        /// check if the acceptor is currently listening for connections
        ///
        /// \return a boolean value
        auto is_open() const noexcept -> bool;

        /// asynchronously accept a tcp connection, i.e. returns immediately
        ///
        /// \param connection is a connection object inside a container of type C
        auto async_accept(C<T> connection) -> bool;

        /// accept a tcp connection, blocking
        ///
        /// \param connection is a connection object inside a container of type C
        auto accept_one(C<T> connection) -> bool;

        /// bind the acceptor to an address and a port and start listening
        ///
        /// \param address is the address to bind to
        /// \param port is the port number to bind to
        /// \return the operation result
        auto bind(std::string_view address, uint16_t port) -> bool;

        /// stop accepting new connections and close the acceptor
        ///
        /// \return the operation result
        auto close() -> bool;
private:
        /// check if a connection was successfully received
        ///
        /// \param error is a boost error
        auto handle_accept(const boost::system::error_code& error) -> void;

        /// decide what to do with a new connection
        ///
        /// \param connection is a connection object inside a container of type C
        auto virtual on_accept(C<T> connection) -> void;

        /// check that the acceptor is ready to accept connections
        ///
        auto is_able_to_accept(C<T> connection) -> bool;

        std::shared_ptr<spdlog::logger> logger;         ///< logger
        boost::asio::ip::tcp::acceptor tcp_acceptor;    ///< boost tcp acceptor
        std::optional<C<T>> listening_connection;       ///< the connection currently used for listening
};

using acceptor_model = acceptor<class defensive, std::shared_ptr>;
WEFTWORKS_DEFENSIVE_FULFILLS_MOVE_ONLY(acceptor_model);

// include implementation
#include "acceptor.tpp"

} // namespace implementation

// lift the implementation
template<class T, template<class...> typename C = std::shared_ptr>
using acceptor = implementation::acceptor<T, C>;

} // namespace weftworks::common::network::tcp
