// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <string_view>
#include <utility>

#include <boost/logic/tribool.hpp>

namespace weftworks::common::network::base {

namespace implementation {

/// parse network requests from data streams
///
class request_parser
{
public:
        /// parse a request from a data stream.
        /// the tribool value returns true when a complete request has been parsed,
        /// false if the request is invalid and indeterminate when more data is needed
        ///
        /// \param begin is an interator pointing to the start of the data stream
        /// \param end is an iterator pointing one place past the end of the data stream
        /// \return a tribool parse result and how much of the data stream was consumed
        template<typename InputIterator>
        auto parse(InputIterator begin, InputIterator end) -> std::pair<boost::tribool, InputIterator>;

        /// parse a request from a string view.
        /// the tribool value returns true when a complete request has been parsed,
        /// false if the request is invalid and indeterminate when more data is needed
        ///
        auto virtual parse(std::string_view data) -> boost::tribool;

        /// parse a request from a character.
        /// the tribool value returns true when a complete request has been parsed,
        /// false if the request is invalid and indeterminate when more data is needed
        ///
        auto parse(char input) -> boost::tribool;
protected:
        //// define how data from the stream should be handled
        ///
        /// \param input is some data to parse
        /// \return a tribool parse result
        auto virtual consume(char input) -> boost::tribool = 0;

        /// check if data value is a character
        ///
        /// \param c is some data
        /// \return a boolean result
        auto is_char(int c) const -> bool;

        /// check if the data value is a digit
        ///
        /// \param c is some data
        /// \return a boolean result
        auto is_digit(int c) const -> bool;
};

// include implementation
#include "request_parser.tpp"

} // namespace implementation

// lift the implementation
using request_parser = implementation::request_parser;

} // namespace weftworks::common::network::base
