// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

template<class T>
inline network_manager<T>::network_manager(boost::asio::io_context& io_context)
        : io_context{io_context}
        , tcp_acceptor{io_context, *this}
        , pool{nullptr}
        , conn_manager{io_context}
{
        logger = spdlog::get("network");
        if (not logger)
                logger = spdlog::default_logger()->clone("network");
}

template<class T>
inline network_manager<T>::network_manager(network_manager&&)
{ }

template<class T>
auto inline network_manager<T>::operator=(network_manager&&) -> network_manager&
{ }

template<class T>
auto inline network_manager<T>::start_network(std::string_view address, uint16_t port) -> bool
{
        if (not pool)
                pool = std::make_shared<managed_connection_pool>(io_context, *this, 0, 500);

        if (tcp_acceptor.bind(address, port)) {
                return start_accept();
        }

        return false;
}

template<class T>
auto inline network_manager<T>::stop_network() -> bool
{
        if (not is_running())
                return false;

        if (not tcp_acceptor.close())
                return false;

        conn_manager.close_connections();

        return true;
}

template<class T>
auto inline network_manager<T>::start_accept() -> bool
{
        if (not is_running())
                return false;

        auto connection = pool->get();
        if (not connection) {
                logger->warn("Max connections reached.");
                return false;
        }

        return tcp_acceptor.start_accept(std::move(connection.value()));
}

template<class T>
inline network_manager<T>::managed_tcp_acceptor::managed_tcp_acceptor(boost::asio::io_context& io_context, network_manager<T>& manager)
        : tcp::acceptor<T, managed_connection_ptr>(io_context)
        , manager{manager}
{ }

template<class T>
auto inline network_manager<T>::managed_tcp_acceptor::on_accept(managed_connection_ptr<T> connection) -> void
{
        if (connection) {
                connection->start();
                manager.conn_manager.add(std::move(connection));
        }
        manager.start_accept();
}

template<class T>
inline network_manager<T>::managed_connection_pool::managed_connection_pool(boost::asio::io_context& io_context, network_manager<T>& manager, size_t init_size, size_t max_size)
        : connection_pool<T>(io_context, init_size, max_size)
        , manager{manager}
{ }

template<class T>
auto inline network_manager<T>::managed_connection_pool::on_return() -> void
{
        logger->trace("A connection returned back to the pool.");
}

template<class T>
auto inline network_manager<T>::is_running() -> bool
{
        return pool ? tcp_acceptor.is_open() : false;
}
