// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

namespace weftworks::common::openflow::group {

/// openflow group commands
///
enum class command : uint16_t
{
        add     = 0x0000, ///< add a new group
        modify  = 0x0001, ///< modify matching groups
        remove  = 0x0002, ///< delete matching groups
};

/// openflow group types
///
enum class type : uint8_t
{
        all             = 0x00, ///< all group
        select          = 0x01, ///< select group
        indirect        = 0x02, ///< indirect group
        fast_failover   = 0x03, ///< fast failover group
};

} // namespace weftworks::common::openflow::group
