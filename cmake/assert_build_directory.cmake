# ./cmake/assert_build_directory.cmake

function(assert_build_directory)
        get_filename_component(SOURCE_DIR "${CMAKE_SOURCE_DIR}" REALPATH)
        get_filename_component(BINARY_DIR "${CMAKE_BINARY_DIR}" REALPATH)

        if(NOT ${SOURCE_DIR}/build STREQUAL ${BINARY_DIR})
                string(ASCII 27 Esc)
                message("${Esc}[1m")
                message(" In-source builds are not allowed. ")
                message(" Remove the CMakeCache.txt file and run cmake .. inside build/ directory.\n ")
                message(" Example: ")
                message(" $ rm CMakeCache.txt ")
                message(" $ mkdir build ")
                message(" $ cd build ")
                message(" $ cmake .. ")
                message(" $ make ")
                message(" ${Esc}[m ")
                message(FATAL_ERROR "")
        endif()
endfunction()
