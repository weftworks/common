// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <memory>
#include <string_view>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/placeholders.hpp>
#include <spdlog/spdlog.h>

#include "utility.hpp"

#include "acceptor.hpp"
#include "connection_manager.hpp"
#include "connection_pool.hpp"

namespace weftworks::common::network::tcp {
namespace implementation {

/// accept and manage tcp connections
///
template<class T>
class network_manager
        : public weftworks::common::utility::noncopyable
{
public:
        /// constructor
        ///
        /// \param io_context is a reference to a boost asio io context
        explicit network_manager(boost::asio::io_context& io_context);

        /// move constructor
        ///
        network_manager(network_manager&&);

        /// move assignment operator
        ///
        auto operator=(network_manager&&) -> network_manager&;

        /// destructor
        ///
        virtual ~network_manager() = default;

        /// start accepting connections
        ///
        /// \param address is the address to bind to
        /// \param port is the port number to bind to
        /// \return the operation result
        auto start_network(std::string_view address, uint16_t port) -> bool;

        /// stop accepting new connections
        ///
        /// \return the operation result
        auto stop_network() -> bool;
protected:
        /// create a new connection object and start accepting connections on it
        ///
        /// \return the operation result
        auto start_accept() -> bool;
private:
        /// get notifications when connections are received
        ///
        class managed_tcp_acceptor
                : public tcp::acceptor<T, tcp::managed_connection_ptr>
        {
        public:
                /// constructor
                ///
                /// \param io_context is a reference to a boost asio io context
                /// \param manager is a reference to a parent network manager
                managed_tcp_acceptor(boost::asio::io_context& io_context, network_manager<T>& manager);
        private:
                /// notify the manager that a connection was received
                ///
                /// \param connection is a connection object which the connection was accepted on
                auto on_accept(tcp::managed_connection_ptr<T> connection) -> void final;

                network_manager<T>& manager; ///< parent network manager
        };

        /// get notifications when connections are closed
        ///
        class managed_connection_pool
                : public tcp::connection_pool<T>
        {
        public:
                /// constructor
                ///
                /// \param io_context is a reference to a boost asio io context
                /// \param manager is a reference to a parent network manager
                /// \param init_size is the amount of connection objects to create on initialization
                /// \param max_size is the maximum size of the pool
                managed_connection_pool(boost::asio::io_context& io_context, network_manager<T>& manager, size_t init_size, size_t max_size);
        private:
                /// notify the manager that a connection was closed and returned to the pool
                ///
                auto on_return() -> void final;

                network_manager<T>& manager; ///< parent network manager
        };

        /// check whether the network is up and running
        ///
        /// \param return a boolean value
        auto is_running() -> bool;

        std::shared_ptr<spdlog::logger> logger;                                 ///< logger
        boost::asio::io_context& io_context;                                    ///< reference to a boost asio io context
        managed_tcp_acceptor tcp_acceptor;                                      ///< a tcp acceptor
        std::shared_ptr<managed_connection_pool> pool;                          ///< a tcp connection pool
        tcp::connection_manager<T, tcp::managed_connection_ptr> conn_manager;   ///< a tcp connection manager
};

WEFTWORKS_DEFENSIVE_FULFILLS_MOVE_ONLY(network_manager<class defensive>);

// include implementation
#include "network_manager.tpp"

} // namespace implementation

// lift the implementation
template<class T>
using network_manager = implementation::network_manager<T>;

} // namespace weftworks::common::network::tcp
