// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

namespace weftworks::common::openflow::port {

/// physical port configuration flags
///
enum class config : uint32_t
{
        port_down       = 0x00000001, ///< port is administratively down
        drop_all        = 0x00000004, ///< drop all packets
        drop_forwarded  = 0x00000020, ///< drop all forwarded packets
        no_packet_in    = 0x00000040, ///< do not send packet_in messages from port
};

/// port features
///
enum class feature : uint32_t
{
        10_mb_half_duplex       = 0x00000001, ///< 10 Mb half-duplex rate support
        10_mb_full_duplex       = 0x00000002, ///< 10 Mb full-duplex rate support
        100_mb_half_duplex      = 0x00000004, ///< 100 Mb half-duplex rate support
        100_mb_full_duplex      = 0x00000008, ///< 100 Mb full-duplex rate support
        1_gb_half_duplex        = 0x00000010, ///< 1 Gb half-duplex rate support
        1_gb_full_duplex        = 0x00000020, ///< 1 Gb full-duplex rate support
        10_gb_full_duplex       = 0x00000040, ///< 10 Gb full-duplex rate support
        40_gb_full_duplex       = 0x00000080, ///< 40 Gb full-duplex rate support
        100_gb_full_duplex      = 0x00000100, ///< 100 Gb full-duplex rate support
        1_tb_full_duplex        = 0x00000200, ///< 1 Tb full-duplex rate support
        other                   = 0x00000400, ///< other rate support
        copper                  = 0x00000800, ///< copper medium
        fiber                   = 0x00001000, ///< fiber medium
        auto_negotiation        = 0x00002000, ///< auto-negotiation support
        pause                   = 0x00004000, ///< pause support
        asymmetric_pause        = 0x00008000, ///< asymmetric pause support
};

/// port numbering flags
///
enum class id : uint32_t
{
        max             = 0xffffff00, ///< maximum number of physical and logical ports
        in_port         = 0xfffffff8, ///< send packet out of the input port
        table           = 0xfffffff9, ///< submit packet to the first flow table
        normal          = 0xfffffffa, ///< process packet with normal L2/L3 switching
        flood           = 0xfffffffb, ///< all live ports in a vlan
        all             = 0xfffffffc, ///< all physical ports except input port
        controller      = 0xfffffffd, ///< send to controller
        local           = 0xfffffffe, ///< local openflow port
        any             = 0xffffffff, ///< wildcard port for flow stats and modification
};

/// internal port state flags
///
enum class state : uint32_t
{
        link_down       = 0x00000001, ///< port has no physical link
        blocked         = 0x00000002, ///< port is blocked
        live            = 0x00000004, ///< port is live for fast-failover group
};

} // namespace weftworks::common::openflow::port
