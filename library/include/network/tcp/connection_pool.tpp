// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

template<class T>
inline connection_pool<T>::connection_deleter::connection_deleter(std::shared_ptr<connection_pool<T>> pool)
        : pool{pool}
{ }

template<class T>
auto inline connection_pool<T>::connection_deleter::operator()(T* connection) -> void
{
        if (auto tmp = pool.lock()) {
                tmp->add(std::unique_ptr<T>{connection});
                return;
        }
        std::default_delete<T>{}(connection);
}

template<class T>
inline connection_pool<T>::connection_pool(boost::asio::io_context& io_context, const size_t& init_size, const size_t& max_size)
        : io_context{io_context}
        , available{max_size}
        , max_size{max_size}
        , pool_size{0}
        , pool{}
{
        initialize(init_size);
}

template<class T>
auto inline connection_pool<T>::get() -> std::optional<managed_connection_ptr>
{
        if (empty()) {
                if (not free())
                        return std::nullopt;
                add();
        }
        --available;
        auto instance = std::move(pool.top());
        pool.pop();
        auto deleter = connection_deleter{this->shared_from_this()};
        auto connection = managed_connection_ptr{instance.release(), deleter};
        return {std::move(connection)};
}

template<class T>
auto inline connection_pool<T>::empty() const -> bool
{
        return pool.empty();
}

template<class T>
auto inline connection_pool<T>::free() const -> size_t
{
        return available;
}

template<class T>
auto inline connection_pool<T>::max() const -> size_t
{
        return max_size;
}

template<class T>
auto connection_pool<T>::size() const -> size_t
{
        return pool_size;
}

template<class T>
auto inline connection_pool<T>::initialize(const size_t& size) -> void
{
        for (size_t i = 0; i < size; ++i) {
                add();
        }
}

template<class T>
auto inline connection_pool<T>::add(std::unique_ptr<T> connection) -> void
{
        if (connection) {
                connection->reset();
                ++available;
                on_return();
        } else {
                connection = std::make_unique<T>(io_context);
                ++pool_size;
        }
        pool.push(std::move(connection));
}

template<class T>
auto inline connection_pool<T>::on_return() -> void
{ }
