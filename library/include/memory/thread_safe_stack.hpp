// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <mutex>
#include <utility>
#include <stack>

namespace weftworks::common::memory {

namespace implementation {

/// store objects in a stack
///
template<class T>
class thread_safe_stack
{
public:
        /// get the element at the top
        ///
        /// \return a reference an element
        auto top() -> T&;
        /// check whether the stack is empty
        ///
        auto empty() const -> bool;
        /// get the stack size
        ///
        auto size() const -> size_t;
        /// remove the top element
        ///
        auto pop() -> void;
        /// add an element at the top of the stack
        ///
        auto push(T&& element) -> void;
private:
        std::stack<T> stack;            ///< the underlying stack
        mutable std::mutex mutex;       ///< a mutable mutex
};


template<class T>
auto inline thread_safe_stack<T>::top() -> T&
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return stack.top();
}

template<class T>
auto inline thread_safe_stack<T>::empty() const -> bool
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return stack.empty();
}

template<class T>
auto inline thread_safe_stack<T>::size() const -> size_t
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return stack.size();
}

template<class T>
auto inline thread_safe_stack<T>::pop() -> void
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        if (not stack.empty())
                stack.pop();
}

template<class T>
auto inline thread_safe_stack<T>::push(T&& element) -> void
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        stack.push(std::move(element));
}

} // namespace implementation

// lift the implementation
template<class T>
using thread_safe_stack = implementation::thread_safe_stack<T>;

} // namespace weftworks::common::memory
