// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "network/openflow/header.hpp"

namespace weftworks::common::network::openflow {

namespace implementation {

header::header()
        : openflow_header{0,0,8,0}
        , parser_state(state::parse_version)
{
        logger = spdlog::get("openflow");
        if (not logger)
                logger = spdlog::default_logger()->clone("openflow");
}

header::header(uint8_t version, uint8_t type)
        : openflow_header{version,type,8,0}
        , parser_state(state::parse_version)
{
        logger = spdlog::get("openflow");
        if (not logger)
                logger = spdlog::default_logger()->clone("openflow");
}

auto header::get_header() const -> const common::openflow::header
{
        return openflow_header;
}

auto header::get_length() const -> int
{
        return openflow_header.length;
}

auto header::get_version() const -> uint8_t
{
        return openflow_header.version;
}

auto header::get_message_type() const -> uint8_t
{
        return openflow_header.type;
}

auto header::get_transaction_id() const -> uint32_t
{
        return openflow_header.xid;
}

auto header::set_version(uint8_t version) -> void
{
        openflow_header.version = version;
}

auto header::set_message_type(uint8_t type) -> void
{
        openflow_header.type = type;
}

auto header::consume(char input) -> boost::tribool
{
        logger->trace("Parser state: {}.", (int)parser_state);

        switch(parser_state) {
        case state::parse_version:
                return parse_version(input);
        case state::parse_type:
                return parse_type(input);
        case state::parse_length_1:
        case state::parse_length_2:
                return parse_length(input);
        case state::parse_xid_1:
        case state::parse_xid_2:
        case state::parse_xid_3:
        case state::parse_xid_4:
                return parse_xid(input);
        default:
                break;
        }

        return false;
}

auto header::reset() -> void
{
        parser_state = state::parse_version;
}

auto header::parse_version(char input) -> boost::tribool
{
        using version = common::openflow::version;

        // check that the input is a valid openflow version
        if (input >= +version::openflow10 and input <= +version::latest) {
                openflow_header.version |= (uint8_t)input;
                logger->trace("Parsed protocol version: {}.", +openflow_header.version);
                parser_state = state::parse_type;

        } else {
                logger->trace("Invalid version received.");
        }

        return boost::indeterminate;
}

auto header::parse_type(char input) -> boost::tribool
{
        using message = common::openflow::message;

        if (input >= +message::hello and input <= +message::max_value) {
                openflow_header.type |= (uint8_t)input;
                parser_state = state::parse_length_1;
                logger->trace("Parsed message type: {}.", +openflow_header.type);
                return boost::indeterminate;
        } else {
                logger->trace("Invalid OpenFlow message type received.");
                return false;
        }
}

auto header::parse_length(char input) -> boost::tribool
{
        switch (parser_state) {
        case state::parse_length_1:
                openflow_header.length |= ((uint8_t)input << 8);
                parser_state = state::parse_length_2;
                return boost::indeterminate;
        case state::parse_length_2:
                openflow_header.length |= (uint8_t)input;
                parser_state = state::parse_xid_1;
                logger->trace("Header length parsed: {}", openflow_header.length);
                return boost::indeterminate;
        default:
                return false;
        }
}

auto header::parse_xid(char input) -> boost::tribool
{
        logger->trace("Header xid input: {}", input);
        switch (parser_state) {
        case state::parse_xid_1:
                openflow_header.xid |= ((uint8_t)input << 24);
                parser_state = state::parse_xid_2;
                logger->trace("Header xid 1 parsed.");
                break;
        case state::parse_xid_2:
                openflow_header.xid |= ((uint8_t)input << 16);
                parser_state = state::parse_xid_3;
                logger->trace("Header xid 2 parsed.");
                break;
        case state::parse_xid_3:
                openflow_header.xid |= ((uint8_t)input << 8);
                parser_state = state::parse_xid_4;
                logger->trace("Header xid 3 parsed.");
                break;
        case state::parse_xid_4:
                openflow_header.xid |= (uint8_t)input;
                logger->trace("Header xid 4 parsed.");
                return true;
        default:
                return false;
        }
        return boost::indeterminate;
}

} // namespace implementation

} // namespace weftworks::common::network::openflow
