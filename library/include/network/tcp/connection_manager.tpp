// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


template<class T, template<class...> typename C>
inline connection_manager<T, C>::connection_manager(boost::asio::io_context& io_context)
        : cleanup_timer{io_context}
        , connections{}
{
        run_cleanup(1);
}

template<class T, template<class...> typename C>
auto inline connection_manager<T, C>::count() -> size_t
{
        return connections.size();
}

template<class T, template<class...> typename C>
auto inline connection_manager<T, C>::add(C<T> connection) -> void
{
        connections.emplace_back(std::move(connection));
}

template<class T, template<class...> typename C>
auto inline connection_manager<T, C>::close_connections() -> void
{
        for (auto& connection : connections)
                connection->close();
        connections.clear();
}

template<class T, template<class...> typename C>
auto inline connection_manager<T, C>::do_cleanup(const boost::system::error_code& error) -> void
{
        if (error)
                return;

        // do cleanup
        auto handler = [](auto const& connection)
        {
                if (not connection->is_open())
                        return connection->close();
                else
                        return false;
        };
        connections.erase(std::remove_if(connections.begin(), connections.end(), handler), connections.end());

        // run the timer again
        run_cleanup(1);
}


template<class T, template<class...> typename C>
auto inline connection_manager<T, C>::run_cleanup(const uint32_t seconds) -> void
{
        cleanup_timer.expires_from_now(boost::posix_time::seconds(seconds)); // TODO: from config file?
        cleanup_timer.async_wait(
                std::bind(&connection_manager<T, C>::do_cleanup, this, std::placeholders::_1)
        );
}
