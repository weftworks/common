// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <catch2/catch.hpp>

#include "network/openflow/message.hpp"
#include "openflow/openflow13.hpp"

namespace openflow13 = weftworks::common::openflow::openflow13;

TEMPLATE_TEST_CASE("openflow network message", "[require]"
        , openflow13::message::hello
        , openflow13::message::error
        // , openflow13::message::echo_request
        // , openflow13::message::echo_response
        // , openflow13::message::experimenter
        , openflow13::message::get_switch_features_request
        //, openflow13::message::get_switch_features_response
        // , openflow13::message::get_switch_config_request
        // , openflow13::message::get_switch_config_response
        // , openflow13::message::set_switch_config
        // , openflow13::message::packet_in
        // , openflow13::message::flow_removed
        // , openflow13::message::port_status
        // , openflow13::message::packet_out
        // , openflow13::message::flow_modification
        // , openflow13::message::group_modification
        // , openflow13::message::port_modification
        // , openflow13::message::table_modification
        // , openflow13::message::multipart_request
        // , openflow13::message::multipart_response
        // , openflow13::message::barrier_request
        // , openflow13::message::barrier_response
        // , openflow13::message::get_queue_config_request
        // , openflow13::message::get_queue_config_response
        // , openflow13::message::controller_role_request
        // , openflow13::message::controller_role_response
        // , openflow13::message::get_async_messages_request
        // , openflow13::message::get_async_messages_response
        // , openflow13::message::set_async_messages
        // , openflow13::message::meter_modification
        )
{
        auto openflow_message = TestType{};
        auto network_message = weftworks::common::network::openflow::message{openflow_message};

        SECTION("version method returns the correct openflow version")
        {
                REQUIRE(network_message.get_version());
                REQUIRE((uint8_t)network_message.get_version().value() == openflow_message.header.version);
        }

        SECTION("type method returns the correct message type")
        {
                REQUIRE(network_message.get_type());
                REQUIRE((uint8_t)network_message.get_type().value() == openflow_message.header.type);
        }

        SECTION("data method correctly serializes the header")
        {
                REQUIRE(network_message.get_data());
                REQUIRE(not network_message.get_data().value().empty());

                auto data = network_message.get_data().value();
                auto new_message = weftworks::common::network::openflow::message{};

                new_message.parse_header(data);

                REQUIRE(new_message.get_header());

                auto header = new_message.get_header().value();

                REQUIRE(openflow_message.header.version == header.version);
                REQUIRE(openflow_message.header.type == header.type);
                REQUIRE(openflow_message.header.length == header.length);
                REQUIRE(openflow_message.header.xid == header.xid);
        }
}
