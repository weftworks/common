// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <vector>

#include "header.hpp"

namespace weftworks::common::openflow::hello {
namespace implementation {
namespace {

// the size of a hello element in bytes
auto constexpr hello_element_header_size = int_fast8_t{4};

} // anonymous namespace

/// store the type and length of a hello element
///
struct alignas(hello_element_header_size) element
{
        uint16_t type;   ///< one of hello types
        uint16_t length; ///< length of this element in bytes
};
static_assert(sizeof(element) == hello_element_header_size);

/// list supported openflow versions
///
struct version_bitmap
        : public element
{
        std::vector<uint32_t> bitmaps;  ///< list of supported openflow versions
};

} // namespace implementation

/// hello element type
///
enum class type : uint16_t
{
        version_bitmap = 1, ///< bitmap of supported openflow protocol versions
};

using element = implementation::element;
using version_bitmap = implementation::version_bitmap;

} // namespace weftworks::common::openflow::hello
