// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <catch2/catch.hpp>

#include "memory/thread_safe_stack.hpp"

TEMPLATE_TEST_CASE("thread-safe stack", "[template]", int, std::string)
{
        auto stack = weftworks::common::memory::thread_safe_stack<TestType>{};

        REQUIRE(stack.empty());
        REQUIRE(stack.size() == 0);

        SECTION("pushing an element increases size")
        {
                stack.push(TestType{1});
                REQUIRE(stack.size() == 1);
        }

        SECTION("popping an element decreases size")
        {
                for (size_t i = 0; i < 3; ++i)
                        stack.push(TestType{0});
                stack.pop();
                REQUIRE(stack.size() == 2);
        }

        SECTION("popping an element from an empty stack does not decrease size")
        {
                stack.pop();
                REQUIRE(stack.size() == 0);
        }

        SECTION("should return top element")
        {
                stack.push(TestType{3});
                stack.push(TestType{1});
                stack.push(TestType{2});
                REQUIRE(stack.top() == TestType{2});
        }
}
