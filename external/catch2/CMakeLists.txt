# ./external/catch2/CMakeLists.txt

find_package(Git REQUIRED)

# Catch2 version
set(CATCH2_VERSION 2.9.1)

# Catch2 settings
set(CATCH_BUILD_EXAMPLES        FALSE)
set(CATCH_BUILD_TESTING         TRUE)
set(CATCH_INSTALL_DOCS          TRUE)
set(CATCH_INSTALL_HELPERS       TRUE)
set(BUILD_TESTING               TRUE)

# Find Catch2
find_package(
        Catch2 ${CATCH2_VERSION}
        REQUIRED
)

# Make Catch2 target globally available.
if (Catch2_FOUND)
        set_target_properties(
                Catch2::Catch2
                PROPERTIES
                        IMPORTED_GLOBAL TRUE
        )
endif()
