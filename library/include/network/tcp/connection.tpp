// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


template<class T>
inline connection<T>::connection(boost::asio::io_context& io_context)
        : io_context{io_context}
        , closing{false}
        , timeout{io_context}
        , read_buffer{}
{
        socket = std::make_unique<boost::asio::ip::tcp::socket>(io_context);
        logger = spdlog::get("network");
        if (not logger)
                logger = spdlog::default_logger()->clone("network");
}

template<class T>
inline connection<T>::connection(connection&& source)
        : logger{std::move(source.logger)}
        , io_context{source.io_context}
        , closing{source.closing}
        , socket{std::move(source.socket)}
        , timeout{std::move(source.timeout)}
{
        read_buffer.commit(boost::asio::buffer_copy(
                read_buffer.prepare(source.read_buffer.size()),
                source.read_buffer.data()
        ));
}

template<class T>
inline connection<T>::~connection()
{
        stop();
        close();
}

template<class T>
auto inline connection<T>::operator=(connection&& source) -> connection&
{
        read_buffer.commit(boost::asio::buffer_copy(
                read_buffer.prepare(source.read_buffer.size()),
                source.read_buffer.data()
        ));
 }

template<class T>
auto inline connection<T>::get_read_data() -> std::string
{
        auto data = std::string{boost::asio::buffers_begin(read_buffer.data()), boost::asio::buffers_end(read_buffer.data())};
        read_buffer.consume(read_buffer.size());
        return data;
}

template<class T>
auto inline connection<T>::get_remote_address() const -> std::optional<std::string>
{
        auto error = boost::system::error_code{};
        auto endpoint = socket->remote_endpoint(error);
        if (error)
                return {std::nullopt};
        auto address = endpoint.address().to_string(error);
        if (error)
                return {std::nullopt};
        else
                return {address};
}

template<class T>
auto inline connection<T>::get_remote_port() const -> std::optional<uint16_t>
{
        auto error = boost::system::error_code{};
        auto endpoint = socket->remote_endpoint(error);
        if (error)
                return {std::nullopt};
        else
                return {endpoint.port()};
}

template<class T>
auto inline connection<T>::is_open() const -> bool
{
        return socket->is_open() and not closing;
}

template<class T>
auto inline connection<T>::async_connect(std::string_view address, const uint16_t& port, std::function<void(const boost::system::error_code&)> on_connect) -> void
{
        auto error = boost::system::error_code{};
        auto host = boost::asio::ip::make_address(address, error);
        if (error) {
                logger->error("Connection attempt failed: {}.", error.message());
                return;
        }
        socket->async_connect({host, port}, on_connect);
}

template<class T>
auto inline connection<T>::async_read(size_t length, std::function<void(const size_t&)> on_read_handler) -> void
{
        auto handler = [&](const boost::system::error_code& error, size_t bytes_transferred) {
                if (not error) {
                        logger->debug("Received {} bytes from {}:{}.", bytes_transferred, *(get_remote_address()), *(get_remote_port()));
                        if (on_read_handler)
                                on_read_handler(bytes_transferred);
                        else
                                on_read(bytes_transferred);
                } else if (error == boost::asio::error::eof or error == boost::asio::error::connection_reset) {
                        logger->debug("{}:{} disconnected.", *(get_remote_address()), *(get_remote_port()));
                        close();
                } else {
                        logger->warn("Error while reading data from socket: {}.", error.message());
                        close();
                }
        };
        read_buffer.prepare(length);
        boost::asio::async_read(*socket, read_buffer, boost::asio::transfer_exactly(length), handler);
}

template<class T>
auto inline connection<T>::async_write(std::string_view data, std::function<void(const size_t&)> on_write_handler) -> void
{
        auto handler = [&](const boost::system::error_code& error, size_t bytes_transferred) {
                if (error) {
                        logger->debug("Error while trying to write: {}", error.message());
                } else {
                        logger->debug("Sent {} bytes to {}:{}.", bytes_transferred, *(get_remote_address()), *(get_remote_port()));
                        if (on_write_handler)
                                on_write_handler(bytes_transferred);
                        else
                                on_write(bytes_transferred);
                }
        };
        boost::asio::async_write(*socket, boost::asio::buffer(data), handler);
}

template<class T>
auto inline connection<T>::close() -> bool
{
        auto error = boost::system::error_code{};
        socket->close(error);
        if (error) {
                logger->error("An error occurred while closing a socket: {}.", error.message());
                return false;
        }
        logger->debug("Connection closed.");
        on_close();
        return true;
}

template<class T>
auto inline connection<T>::flush() -> void
{
        read_buffer.consume(read_buffer.max_size());
}

template<class T>
auto inline connection<T>::start() -> void
{
        if (not is_open()) {
                logger->error("Tried to start a connection on a closed or closing socket.");
                return;
        }
        logger->debug("Connection from {}:{} received.", *(get_remote_address()), *(get_remote_port()));
        //read();
}

template<class T>
auto inline connection<T>::stop() -> void
{
        closing = true;
}

template<class T>
auto inline connection<T>::reset() -> void
{
        if (close())
                socket = std::make_unique<boost::asio::ip::tcp::socket>(io_context);
        else
                logger->error("Couldn't reset a socket!");
}

template<class T>
auto inline connection<T>::set_timeout(const uint16_t& seconds) -> void
{
        auto handler = [&](const boost::system::error_code& error) {
                // do nothing if the timer was canceled
                if (error == boost::asio::error::operation_aborted) {
                        return;
                } else if (socket->is_open()) {
                        logger->warn("Connection to {} timed out.", *(get_remote_address()));
                        close();
                }
        };
        timeout.expires_from_now(boost::posix_time::seconds(seconds));
        timeout.async_wait(handler);
}

template<class T>
auto inline connection<T>::on_close() -> void
{ }

template<class T>
auto inline connection<T>::on_read(const size_t& /*bytes_transferred*/) -> void
{ }

template<class T>
auto inline connection<T>::on_write(const size_t& /*bytes_transferred*/) -> void
{ }
