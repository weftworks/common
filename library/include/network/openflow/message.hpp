// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <algorithm>
#include <memory>
#include <optional>
#include <string>

#include <boost/logic/tribool.hpp>
#include <spdlog/spdlog.h>

#include "header.hpp"
#include "network/base/request_parser.hpp"
#include "openflow/common.hpp"
#include "openflow/header.hpp"
#include "openflow/openflow13.hpp"

namespace weftworks::common::network::openflow {
namespace implementation {

/// present an openflow message
///
class message
        : public network::base::request_parser
{
public:
        /// constructor
        ///
        /// \param message is an openflow message
        template<typename T>
        explicit message(const T& message);

        /// constructor
        ///
        message();

        /// get the message data in network format
        ///
        /// \return data as a string if any
        auto get_data() const -> std::optional<std::string>;

        /// get the openflow header of the message
        ///
        /// \return an openflow header
        auto get_header() const -> std::optional<common::openflow::header>;

        /// get the message length in bytes
        ///
        auto get_length() const -> int;

        /// get the underlying message
        ///
        auto get_message() const -> std::shared_ptr<void>;

        /// get the openflow message name
        ///
        /// \return an openflow message name as a string if any
        auto get_name() const -> std::optional<std::string>;

        /// get the openflow message type
        ///
        /// \return an openflow message type if any
        auto get_type() const -> std::optional<common::openflow::message>;

        /// get the openflow protocol version
        ///
        /// \return an openflow version if any
        auto get_version() const -> std::optional<common::openflow::version>;

        /// parse an openflow message from a string view.
        /// the tribool value returns true when a complete message has been parsed,
        /// false if the message is invalid and indeterminate when more data is needed
        ///
        auto virtual parse(std::string_view data) -> boost::tribool final;

        /// parse an openflow message header from a string view.
        /// the tribool value returns true when a complete message has been parsed,
        /// false if the message is invalid and indeterminate when more data is needed
        ///
        auto virtual parse_header(std::string_view data) -> boost::tribool final;

        /// parse an openflow message payload from a string view.
        /// the tribool value returns true when a complete message has been parsed,
        /// false if the message is invalid and indeterminate when more data is needed
        ///
        auto virtual parse_payload(std::string_view data) -> boost::tribool final;
private:
        /// present the concept of an openflow message
        ///
        class message_concept
        {
        public:
                /// destructor
                ///
                virtual ~message_concept();

                /// get openflow message data
                ///
                /// \return data as a string if any
                auto virtual get_data() const -> std::optional<std::string> = 0;

                /// get the openflow header of the message
                ///
                /// \return an openflow header
                auto virtual get_header() const -> std::optional<common::openflow::header> = 0;

                /// get message length in bytes
                ///
                auto virtual get_length() const -> int = 0;

                /// get the message
                ///
                auto virtual get_message() const -> std::shared_ptr<void> = 0;

                /// get openflow message name
                ///
                /// \return an openflow message name as a string if any
                auto virtual get_name() const -> std::optional<std::string> = 0;

                /// get openflow message type
                ///
                /// \return an openflow message type if any
                auto virtual get_type() const -> std::optional<common::openflow::message> = 0;

                /// get message openflow version
                ///
                /// \return an openflow version if any
                auto virtual get_version() const -> std::optional<common::openflow::version> = 0;

                /// parse an openflow header
                ///
                /// \param input is some data
                /// \return a tribool operation result
                auto virtual parse_header(char input) -> boost::tribool = 0;
        };

        /// message model
        ///
        template<typename T>
        class message_model
                : public message_concept
        {
        public:
                /// constructor
                ///
                /// \param message is an openflow message
                explicit message_model(const T& message);

                /// destructor
                ///
                virtual ~message_model();

                /// get openflow message data
                ///
                /// \return data as a string if any
                auto virtual get_data() const -> std::optional<std::string>;

                /// get the openflow header of the message
                ///
                /// \return an openflow header
                auto virtual get_header() const -> std::optional<common::openflow::header>;

                /// get message length in bytes
                ///
                auto virtual get_length() const -> int;

                /// get the message
                ///
                auto virtual get_message() const -> std::shared_ptr<void>;

                /// get openflow message name
                ///
                /// \return an openflow message name as a string if any
                auto virtual get_name() const -> std::optional<std::string>;

                /// get openflow message type
                ///
                /// \return an openflow message type if any
                auto virtual get_type() const -> std::optional<common::openflow::message>;

                /// get message openflow version
                ///
                /// \return an openflow version if any
                auto virtual get_version() const -> std::optional<common::openflow::version>;

                /// parse an openflow header
                ///
                /// \param input is some data
                /// \return a tribool operation result
                auto virtual parse_header(char input) -> boost::tribool;

                std::shared_ptr<spdlog::logger> logger; ///< logger
                T message;                              ///< stored openflow message
        };

        /// present parser state
        ///
        enum class state
        {
                parse_header,
                parse_payload
        };

        /// parse an openflow message one byte at a time
        ///
        /// \param input is some data to parse
        /// \return a tribool operation result
        auto consume(char input) -> boost::tribool final;

        /// parse an openflow header
        ///
        /// \param input is some data
        /// \return a tribool operation result
        auto parse_header(char input) -> boost::tribool;

        /// reset the parser state
        ///
        auto reset_parser_state() -> void;

        std::shared_ptr<spdlog::logger> logger; ///< logger
        std::shared_ptr<message_concept> msg;   ///< an openflow message
        state parser_state;                     ///< current parser state
};

/// check that an openflow header is valid
///
/// \param header is an openflow header
auto inline is_valid_header(const common::openflow::header& header) -> bool;

/// check that an openflow header type is valid
///
/// \param type is an openflow header type
auto inline is_valid_type(uint8_t type) -> bool;

/// check that an openflow header version is valid
///
/// \param version is an openflow header version
auto inline is_valid_version(uint8_t version) -> bool;

// include implementation
#include "message.tpp"

} // namespace implementation

// lift the implementation
using message = implementation::message;

} // namespace weftworks::common::network::openflow
