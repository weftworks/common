// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include "openflow/common.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {

namespace implementation {

/// present a meter band
///
struct meter_band
{
        uint16_t type;                  ///< meter band type
        uint16_t length;                ///< band length in bytes
        uint32_t rate;                  ///< band rate
        uint32_t burst_size;            ///< burst size
        uint32_t experimenter_id;       ///< experimenter id
        // TODO!
};

/// present a meter modification message
///
struct meter_modification
{
        /// constructor
        ///
        meter_modification();

        openflow::header header;        ///< openflow header
        uint16_t command;               ///< meter modification command
        uint16_t flags;                 ///< meter modification flags
        uint32_t meter_id;              ///< meter identifier
        std::vector<meter_band> bands;  ///< meter bands

};

} // namespace implementation

// lift the implementation
using meter_modification = implementation::meter_modification;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const meter_modification& message) -> std::string;

} // namespace weftworks::common::openflow::openflow13::message
