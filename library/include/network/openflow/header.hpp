// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <boost/logic/tribool.hpp>
#include <spdlog/spdlog.h>

#include "network/base/request_parser.hpp"
#include "openflow/openflow.hpp"

namespace weftworks::common::network::openflow {
namespace implementation {

/// construct and parse openflow headers
///
class header
        : public network::base::request_parser
{
public:
        /// constructor
        ///
        header();

        /// constructor
        ///
        /// \param version is the openflow protocol version
        /// \param type is the openflow message type
        header(uint8_t version, uint8_t type);

        /// get the underlying openflow header
        ///
        auto get_header() const -> const common::openflow::header;

        /// get the message length in bytes
        ///
        auto get_length() const -> int;

        /// get the openflow protocol version
        ///
        /// \return a openflow protocol version
        auto get_version() const -> uint8_t;

        /// get the openflow message type
        ///
        /// \return an openflow message type
        auto get_message_type() const -> uint8_t;

        /// get the header transaction identifier
        ///
        /// \return an transaction identifier
        auto get_transaction_id() const -> uint32_t;

        /// set the openflow protocol version for this header
        ///
        /// \param version is the protocol version
        auto set_version(uint8_t version) -> void;

        /// set the openflow message type for this header
        ///
        /// \param type is the message type
        auto set_message_type(uint8_t type) -> void;
protected:
        /// parse an openflow header one byte at a time
        ///
        /// \param input is some data to parse
        /// \return a tribool operation result
        auto consume(char input) -> boost::tribool final;

        /// reset the parser state
        ///
        auto reset() -> void;
private:
        /// present parser state
        ///
        enum class state
        {
                parse_version,
                parse_type,
                parse_length_1,
                parse_length_2,
                parse_xid_1,
                parse_xid_2,
                parse_xid_3,
                parse_xid_4
        };

        /// parse the openflow protocol version
        ///
        /// \param input is some data
        /// \return tribool operation result
        auto parse_version(char input) -> boost::tribool;

        /// parse the openflow message type
        ///
        /// \param input is some data
        /// \return a tribool operation result
        auto parse_type(char input) -> boost::tribool;

        /// parses the openflow message length
        ///
        /// \param input is some data
        /// \return a tribool operation result
        auto parse_length(char input) -> boost::tribool;

        /// parse the openflow transaction identifier one byte at a time.
        ///
        /// \param input is some data
        /// \return a tribool operation result
        auto parse_xid(char input) -> boost::tribool;

        std::shared_ptr<spdlog::logger> logger;         ///< logger
        common::openflow::header openflow_header;       ///< stored header
        state parser_state;                             ///< current parser state
};

} // namespace implementation

// lift the implementation
using header = implementation::header;

} // namespace weftworks::common::network::openflow
