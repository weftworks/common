// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <stdint.h>

namespace weftworks::common::openflow {
namespace implementation {
namespace {

// the size of an openflow header in bytes
auto constexpr openflow_header_size = int_fast8_t{8};

} // anonymous namespace

/// common header used in every openflow message independent of protocol version.
///
struct alignas(openflow_header_size) header
{
        /// constructor
        ///
        header();

        /// constructor
        ///
        /// \param version_ is an openflow protocol version
        /// \param type_ is an openflow message type
        /// \param length_ is the total length of an openflow message
        /// \param xid_ is a transaction identifier
        header(uint8_t version_, uint8_t type_, uint16_t length_, uint32_t xid_);

        uint8_t version; ///< protocol version
        uint8_t type;    ///< message type
        uint16_t length; ///< length of the message including this header
        uint32_t xid;    ///< transaction identifier used to match requests to responses
};
static_assert(sizeof(header) == openflow_header_size);

} // namespace implementation

// lift the implementation
using header = implementation::header;

} // namespace weftworks::common::openflow
