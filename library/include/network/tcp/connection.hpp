// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <array>
#include <memory>
#include <optional>

#include <boost/asio.hpp>
#include <spdlog/spdlog.h>

#include "utility.hpp"

namespace weftworks::common::network::tcp {
namespace implementation {

/// send and receive data over a network
///
template<class T>
class connection
        : public utility::noncopyable
{
public:
        /// constructor
        ///
        /// \param io_context is a reference to a boost asio io context
        explicit connection(boost::asio::io_context& io_context);

        /// move constructor
        ///
        connection(connection&& source);

        /// deconstructor
        ///
        ~connection();

        /// move assignment operator
        ///
        auto operator=(connection&& source) -> connection&;

        /// return a string view of the read buffer data
        ///
        /// \return read buffer contents
        auto get_read_data() -> std::string;

        /// return the remote endpoint address
        ///
        /// \return a network address
        auto get_remote_address() const -> std::optional<std::string>;

        /// return the remote endpoint port
        ///
        /// \return a port number
        auto get_remote_port() const -> std::optional<uint16_t>;

        /// determine whether the connection is open or closed
        ///
        /// \return a boolean result
        auto is_open() const -> bool;

        /// asynchronously connect to an endpoint with a callback
        ///
        /// \param address is a network address
        /// \param port is a port number
        /// \param on_connect is a callback function to call after a connection is made
        auto virtual async_connect(std::string_view address, const uint16_t& port, std::function<void(const boost::system::error_code&)> on_connect) -> void;

        /// read some data from the socket
        ///
        /// \param length is the minimum number of bytes to read
        /// \param on_read_handler is the callback function to call after read
        auto virtual async_read(size_t length, std::function<void(const size_t&)> on_read_handler = {}) -> void;

        /// write some data to the socket
        ///
        /// \param data reference to a data buffer
        /// \param on_write_handler is the callback function to call after write
        auto virtual async_write(std::string_view data, std::function<void(const size_t&)> on_write_handler = {}) -> void;

        /// close the tcp connection
        ///
        /// \return the operation result
        auto virtual close() -> bool;

        /// flush the data buffer
        ///
        auto flush() -> void;

        /// start the connection data exchange
        ///
        auto virtual start() -> void;

        /// mark the connection as closing
        ///
        auto virtual stop() -> void;

        /// reset the connection
        ///
        auto reset() -> void;

        /// set a connection timeout
        ///
        /// \param seconds is timeout in seconds
        auto set_timeout(const uint16_t& seconds) -> void;

        /// allow acceptor to access the underlying tcp socket
        ///
        template<class, template<class...> typename>
        friend class acceptor;
private:
        /// noop on connection close event
        ///
        auto virtual on_close() -> void;

        /// handle on_read event
        ///
        auto virtual on_read(const size_t& bytes_transferred) -> void;

        /// handle on_write event
        ///
        auto virtual on_write(const size_t& bytes_transferred) -> void;

        std::shared_ptr<spdlog::logger> logger;                 ///< logger
        boost::asio::io_context& io_context;                    ///< reference to a boost asio io context
        bool closing;                                           ///< connection status
        std::unique_ptr<boost::asio::ip::tcp::socket> socket;   ///< boost asio tcp socket
        boost::asio::deadline_timer timeout;                    ///< boost asio timer
        boost::asio::streambuf read_buffer;                     ///< data buffer
};

WEFTWORKS_DEFENSIVE_FULFILLS_MOVE_ONLY(connection<class defensive>);

// include implementation
#include "connection.tpp"

} // namespace implementation

// lift the implementation
template<class T>
using connection = implementation::connection<T>;

} // namespace weftworks::common::network::tcp
