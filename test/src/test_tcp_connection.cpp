// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <string_view>

#include <boost/array.hpp>
#include <catch2/catch.hpp>

#include "network/tcp/acceptor.hpp"
#include "test/network/tcp/connection.hpp"

namespace tcp = weftworks::common::network::tcp;
namespace test = weftworks::common::test::network::tcp;

TEMPLATE_TEST_CASE("tcp connection", "[network][template][require]", test::connection)
{
        auto io_context = boost::asio::io_context{};

        auto acceptor = tcp::acceptor<TestType>{io_context};

        auto server = std::make_shared<TestType>(io_context);
        auto client = std::make_shared<TestType>(io_context);

        const auto address = std::string_view{"127.0.0.1"};
        auto port = uint16_t{0};

        acceptor.bind(address, port);
        acceptor.start_accept(server);

        auto handler = [&](const boost::system::error_code& /*error*/) {
                io_context.stop();
        };

        auto msg = std::string_view{"a cat crossed the road"};
        port = acceptor.get_port();

        SECTION("should be closed on creation") {
                REQUIRE(not client->is_open());
        }

        SECTION("should able to return connection status") {
                REQUIRE(not server->is_open());
                client->async_connect(address, port, handler);
                io_context.run();
                io_context.restart();
                REQUIRE(server->is_open());
        }

        SECTION("should able to be closed") {
                client->async_connect(address, port, handler);
                io_context.run();
                io_context.restart();
                REQUIRE(client->is_open());
                client->close();
                REQUIRE(not client->is_open());
        }

        SECTION("should return remote address / should able to connect to an endpoint") {
                REQUIRE(not client->get_remote_address());
                client->async_connect(address, port, handler);
                io_context.run();
                io_context.restart();
                REQUIRE(client->get_remote_address());
                REQUIRE(*client->get_remote_address() == address);
        }

        SECTION("should return the remote port") {
                REQUIRE(not client->get_remote_port());
                client->async_connect(address, port, handler);
                io_context.run();
                io_context.restart();
                REQUIRE(*client->get_remote_port() == port);
        }

        SECTION("should able to write to a socket / should be able to read from a socket") {
                client->async_connect(address, port, handler);
                io_context.run();
                io_context.restart();
                REQUIRE(client->is_open());
                client->write(msg);
                io_context.run();
                io_context.restart();
                server->read(msg.length());
                REQUIRE(server->get_read_data() == msg);
        }

        SECTION("should be marked for closing") {
                client->async_connect(address, port, handler);
                io_context.run();
                io_context.restart();
                REQUIRE(client->is_open());
                client->stop();
                REQUIRE(not client->is_open());
        }
}
