// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iomanip>
#include <sstream>
#include <string>

#include "openflow/common.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {

namespace implementation {

/// present a flow entry modification message
///
struct flow_modification
{
        /// constructor
        ///
        flow_modification();

        openflow::header header;        ///< openflow header
        uint64_t cookie;                ///< flow entry identifier given by a controller
        uint64_t cookie_mask;           ///< cookie restriction mask
        uint8_t table_id;               ///< flow entry table id
        uint8_t command;                ///< flow entry modification command
        uint16_t idle_timeout;          ///< flow entry idle timeout before discarding
        uint16_t hard_timeout;          ///< flow entry hard timeout before discarding
        uint16_t priority;              ///< flow entry priority
        uint32_t buffer_id;             ///< packet id from packet_in to apply the flow to
        uint32_t out_port;              ///< flow output port
        uint32_t out_group;             ///< flow output group
        uint16_t flags;                 ///< flow entry flags
        uint16_t padding;               ///< data padding
        uint32_t match;                 ///< flow entry match fields
        uint32_t instruction;           ///< flow instructions
};

} // namespace implementation

// lift the implementation
using flow_modification = implementation::flow_modification;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const flow_modification& message) -> std::string;

} // namespace weftworks::common::openflow::openflow13::message
