// BSD 3-Clause License
//
// Original work Copyright (C) 2018 Philippe Groarke.
// Modified work Copyright (C) 2018 Antti Lohtaja
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <type_traits>

namespace weftworks::common::utility {

/// defensive traits
///
/// check all required type_traits and compose rules with them.
/// since static_asserts cannot be in constexpr branches, the rules get a little iffy.
/// we need to make sure every rule only triggers in certain conditions, which is
/// unintuitive but required
template<class t>
struct defensive {
        /// ensure rule of 5
        ///
        /// first we make sure all contructors are present, if not then bail out
        /// since you can't fulfill rule of 5 if you have some constructors missing.
        /// if all constructors are present, we check whether they are all trivial or
        /// all non-trivial. due to a c++ bug, manually implementing a destructor
        /// will bypass checking the copy and move constructors. nothing we can do
        /// about that :(
        struct five {
                /// check that all constructors are present
                ///
                /// @return a boolean value
                auto static constexpr generated_constructors() -> bool {
                        return {
                                destructible
                                and copy_constructible
                                and move_constructible
                                and copy_assignable
                                and move_assignable
                        };
                };
                /// check that all constructors are trivial
                ///
                /// @return a boolean value
                auto static constexpr all_trivial() -> bool {
                        return (trivially_destructible
                                and trivially_copy_constructible
                                and trivially_copy_assignable
                                and trivially_move_constructible
                                and trivially_move_assignable);
                };
                /// check that all constructors are non-trivial
                ///
                /// @return a boolean value
                auto static constexpr all_non_trivial() -> bool {
                        return (not trivially_destructible
                                and not trivially_copy_constructible
                                and not trivially_copy_assignable
                                and not trivially_move_constructible
                                and not trivially_move_assignable);
                };
                /// rule of 5 pass
                ///
                /// @return a boolean value
                auto static constexpr rule_pass() -> bool {
                        // if we don't have 5 constructors, don't trigger static_assert
                        // for rule of 5 user defined constructors. that error will be
                        // caught by another static assert
                        return not generated_constructors() or all_trivial() or all_non_trivial();
                };
                /// check the destructor
                ///
                /// @return a boolean value
                auto static constexpr user_destructor_ok() -> bool {
                        return rule_pass() or not trivially_destructible;
                };
                /// check the copy constructor
                ///
                /// @return a boolean value
                auto static constexpr user_copy_constructor_ok() -> bool {
                        return rule_pass() or not trivially_copy_constructible;
                };
                /// check the move constructor
                ///
                /// @return a boolean value
                auto static constexpr user_move_constructor_ok() -> bool {
                        return rule_pass() or not trivially_move_constructible;
                };
                /// check for the copy assignment operator
                ///
                /// @return a boolean value
                auto static constexpr user_copy_assignment_ok() -> bool {
                        return rule_pass() or not trivially_copy_assignable;
                };
                /// check for the move assignment operator
                ///
                /// @return a boolean value
                auto static constexpr user_move_assignment_ok() -> bool {
                        return rule_pass() or not trivially_move_assignable;
                };
        };
        /// ensure rule of 5 + require default constructor
        ///
        struct six {
                /// check that a class has a default constructor and fulfills the rule of five
                ///
                /// @return a boolean value
                auto static constexpr generated_constructors() -> bool {
                        return default_constructible and five::generated_constructors();
                };
        };
        /// ensure that a class is optimized to be stored in a vector
        ///
        /// checks whether it is trivially destructible (skips destructor call on resize) and
        /// trivially copy constructible (use memcpy on resive). if not, falls back
        /// to ensure your class is noexcept move constructible (vector resize cannot
        /// use your move constructor if it isn't marked noexcept)
        struct fast_vector {
                /// fast vector rule pass
                ///
                /// @return a boolean value
                auto static constexpr rule_pass() -> bool {
                        return ((trivially_copy_constructible and trivially_destructible)
                                or nothrow_move_constructible);
                };
                /// check for a trivial destructor
                ///
                /// @return a boolean value
                auto static constexpr trivial_destructor_ok() -> bool {
                        // only warn if type has trivial copy ctor
                        return (rule_pass() or not trivially_copy_constructible
                                or trivially_destructible);
                };
                /// check for a trivial copy constructor
                ///
                /// @return a boolean value
                auto static constexpr trivial_copy_constructor_ok() -> bool {
                        // only warn if type has trivial destructor
                        return (rule_pass() or not trivially_destructible
                                or trivially_copy_constructible);
                }
                /// check for a move constructor which doesn't throw exceptions
                ///
                /// @return a boolean value
                auto static constexpr nothrow_move_constructor_ok() -> bool {
                        return (rule_pass() or trivially_destructible
                                or trivially_copy_constructible
                                or nothrow_move_constructible);
                };
        };
        /// ensure that a class is move only
        ///
        /// check that there is no copy constructor/operator
        /// and the move constructor/operator is present
        struct move_only {
                /// move only rule pass
                ///
                /// @return a boolean value
                auto static constexpr rule_pass() -> bool {
                        return not copy_constructible and not copy_assignable
                        and move_constructible and move_assignable;
                };
                /// check for the copy constructor
                ///
                /// @return a boolean value
                auto static constexpr copy_constructor_ok() -> bool {
                        return rule_pass() or not copy_constructible;
                };
                /// check for the move constructor
                ///
                /// @return a boolean value
                auto static constexpr move_constructor_ok() -> bool {
                        return rule_pass() or move_constructible;
                };
                /// check for the copy assignment operator
                ///
                /// @return a boolean value
                auto static constexpr copy_assignment_ok() -> bool {
                        return rule_pass() or not copy_assignable;
                };
                /// check for the moe assignment operator
                ///
                /// @return a boolean value
                auto static constexpr move_assignment_ok() -> bool {
                        return rule_pass() or move_assignable;
                };
        };
        /// ensure that a class is non-constructible
        ///
        /// check that an object has no default constructor, destructor,
        /// copy constructor, move constructor, copy operator, move operator
        struct non_constructible {
                /// non-constructible rule pass
                ///
                /// @return a boolean value
                auto static constexpr rule_pass() -> bool {
                        return not default_constructible and not destructible
                        and not copy_constructible and not move_constructible
                        and not copy_assignable and not move_assignable;
                };
                /// check for the constructor
                ///
                /// @return a boolean value
                auto static constexpr constructor_ok() -> bool {
                        return rule_pass() or not default_constructible;
                };
                /// check for the destructor
                ///
                /// @return a boolean value
                auto static constexpr destructor_ok() -> bool {
                        return rule_pass() or not destructible;
                };
                /// check for the copy constructor
                ///
                /// @return a boolean value
                auto static constexpr copy_constructor_ok() -> bool {
                        return rule_pass() or not copy_constructible;
                };
                /// check for the move constructor
                ///
                /// @return a boolean value
                auto static constexpr move_constructor_ok() -> bool {
                        return rule_pass() or not move_constructible;
                };
                /// check for the copy assignment operator
                ///
                /// @return a boolean value
                auto static constexpr copy_assignment_ok() -> bool {
                        return rule_pass() or not copy_assignable;
                };
                /// check for the move assignment operator
                ///
                /// @return a boolean value
                auto static constexpr move_assignment_ok() -> bool {
                        return rule_pass() or not move_assignable;
                };
        };

        // required traits computed once
        auto static constexpr default_constructible = std::is_default_constructible<t>::value;                          ///< default constructible
        auto static constexpr trivially_default_constructible = std::is_trivially_default_constructible<t>::value;      ///< trivially default constructible
        auto static constexpr destructible = std::is_destructible<t>::value;                                            ///< destructible
        auto static constexpr trivially_destructible = std::is_trivially_destructible<t>::value;                        ///< trivially destructible
        auto static constexpr copy_constructible = std::is_copy_constructible<t>::value;                                ///< copy constructible
        auto static constexpr trivially_copy_constructible = std::is_trivially_copy_constructible<t>::value;            ///< trivially copy constructible
        auto static constexpr move_constructible = std::is_move_constructible<t>::value;                                ///< move constructible
        auto static constexpr trivially_move_constructible = std::is_trivially_move_constructible<t>::value;            ///< trivially move constructible
        auto static constexpr nothrow_move_constructible = std::is_nothrow_move_constructible<t>::value;                ///< move constructible and doesn't throw exceptions
        auto static constexpr copy_assignable = std::is_copy_assignable<t>::value;                                      ///< copy assignable
        auto static constexpr trivially_copy_assignable = std::is_trivially_copy_assignable<t>::value;                  ///< trivially copy assignable
        auto static constexpr move_assignable = std::is_move_assignable<t>::value;                                      ///< move assignable
        auto static constexpr trivially_move_assignable = std::is_trivially_move_assignable<t>::value;                  ///< trivially move assignable
};

} // namespace weftworks::common::utility

/// make sure 5 constructors/operators are present (destructor, copy
/// constructor, move constructor, copy assignment constructor, move assignment
/// constructor). useful when using rule of 0 or when using = default
#define WEFTWORKS_DEFENSIVE_FULFILLS_5_CONSTRUCTORS(T) \
        static_assert(weftworks::common::utility::defensive<T>::five::generated_constructors(), \
                      #T " : requires destructor, copy and move constructor, copy and move assignment operator"); \
        static_assert(weftworks::common::utility::defensive<T>::destructible, \
                      " - " #T " : must be destructible"); \
        static_assert(weftworks::common::utility::defensive<T>::copy_constructible, \
                      " - " #T " : must be copy constructible"); \
        static_assert(weftworks::common::utility::defensive<T>::move_constructible, \
                      " - " #T " : must be move constructible"); \
        static_assert(weftworks::common::utility::defensive<T>::copy_assignable, \
                      " - " #T " : must be copy assignable"); \
        static_assert(weftworks::common::utility::defensive<T>::move_assignable, \
                      " - " #T " : must be move assignable")

/// make sure a class fulfills rule of 5. all 5 constructors/operators are
/// present (destructor, copy constructor, move constructor, copy assignment
/// operator, move assignment operator). if you implement 1 custom
/// constructor/operator, you probably need to implement all of them
#define WEFTWORKS_DEFENSIVE_FULFILLS_RULE_OF_5(T) \
        WEFTWORKS_DEFENSIVE_FULFILLS_5_CONSTRUCTORS(T); \
        static_assert(weftworks::common::utility::defensive<T>::five::rule_pass(), \
                      #T " : doesn't fulfill rule of 5"); \
        static_assert(weftworks::common::utility::defensive<T>::five::user_destructor_ok(), \
                      " - " #T " : must implement user-defined destructor"); \
        static_assert(weftworks::common::utility::defensive<T>::five::user_copy_constructor_ok(), \
                      " - " #T " : must implement user-defined copy constructor"); \
        static_assert(weftworks::common::utility::defensive<T>::five::user_move_constructor_ok(), \
                      " - " #T " : must implement user-defined move constructor"); \
        static_assert(weftworks::common::utility::defensive<T>::five::user_copy_assignment_ok(), \
                      " - " #T " : must implement user-defined copy assignment operator"); \
        static_assert(weftworks::common::utility::defensive<T>::five::user_move_assignment_ok(), \
                      " - " #T " : must implement user-defined move assignment operator")

/// make sure all 6 constructors/operators are
/// present (default, destructor, copy constructor, move constructor, copy
/// assignment operator, move assignment operator). useful when using rule of 0
/// or = default
#define WEFTWORKS_DEFENSIVE_FULFILLS_6_CONSTRUCTORS(T) \
        static_assert(weftworks::common::utility::defensive<T>::six::generated_constructors(), \
                      #T " : requires default constructor, destructor, copy and move constructor, copy and move assignment operator"); \
        static_assert(weftworks::common::utility::defensive<T>::default_constructible, \
                      " - " #T " : must be default constructible"); \
        static_assert(weftworks::common::utility::defensive<T>::destructible, \
                      " - " #T " : must be destructible"); \
        static_assert(weftworks::common::utility::defensive<T>::copy_constructible, \
                      " - " #T " : must be copy constructible"); \
        static_assert(weftworks::common::utility::defensive<T>::move_constructible, \
                      " - " #T " : must be move constructible"); \
        static_assert(weftworks::common::utility::defensive<T>::copy_assignable, \
                      " - " #T " : must be copy assignable"); \
        static_assert(weftworks::common::utility::defensive<T>::move_assignable, \
                      " - " #T " : must be move assignable")

/// rule of 5 with an extra check to make sure your class has a default
/// constructor. all 6 constructors/operators are present (default constructor,
/// destructor, copy constructor, move constructor, copy assignment operator,
/// move assignment operator). if you implement 1 of 5 custom
/// constructor/operator, you probably need to implement all of them (destructor,
/// copy constructor, move constructor, copy assignment operator, move
/// assignment operator)
#define WEFTWORKS_DEFENSIVE_FULFILLS_RULE_OF_6(T) \
        WEFTWORKS_DEFENSIVE_FULFILLS_6_CONSTRUCTORS(T); \
        static_assert(weftworks::common::utility::defensive<T>::five::rule_pass(), \
                      #T " : doesn't fulfill rule of 5"); \
        static_assert(weftworks::common::utility::defensive<T>::five::user_destructor_ok(), \
                      " - " #T " : must implement user-defined destructor"); \
        static_assert(weftworks::common::utility::defensive<T>::five::user_copy_constructor_ok(), \
                      " - " #T " : must implement user-defined copy constructor"); \
        static_assert(weftworks::common::utility::defensive<T>::five::user_move_constructor_ok(), \
                      " - " #T " : must implement user-defined move constructor"); \
        static_assert(weftworks::common::utility::defensive<T>::five::user_copy_assignment_ok(), \
                      " - " #T " : must implement user-defined copy assignment operator"); \
        static_assert(weftworks::common::utility::defensive<T>::five::user_move_assignment_ok(), \
                      " - " #T " : must implement user-defined move assignment operator")

/// ensure your class is optimized to be stored in a vector. checks whether
/// it is trivially destructible (skips destructor call on resize) and
/// trivially copy constructible (use memcpy on resive). if not, falls back
/// to ensure your class is noexcept move constructible (vector resize cannot
/// use your move constructor if it isn't marked noexcept)
#define WEFTWORKS_DEFENSIVE_FULFILLS_FAST_VECTOR(T) \
        static_assert(weftworks::common::utility::defensive<T>::fast_vector::rule_pass(), \
                      #T " : doesn't fulfill fast vector requirements"); \
        static_assert(weftworks::common::utility::defensive<T>::fast_vector::trivial_destructor_ok(), \
                      " - " #T " : must generate trivial destructor"); \
        static_assert(weftworks::common::utility::defensive<T>::fast_vector::trivial_copy_constructor_ok(), \
                      " - " #T " : must generate trivial copy constructor"); \
        static_assert(weftworks::common::utility::defensive<T>::fast_vector::nothrow_move_constructor_ok(), \
                      " - " #T " : must implement either trivial destructor and trivial copy constructor, or noexcept move constructor")

/// ensure your class is move only. there is no copy constructor and no copy
/// assignment operator. the move constructor and move assignment operator is
/// present
#define WEFTWORKS_DEFENSIVE_FULFILLS_MOVE_ONLY(T) \
        static_assert(weftworks::common::utility::defensive<T>::move_only::rule_pass(), \
                      #T " : doesn't fulfill move only"); \
        static_assert(weftworks::common::utility::defensive<T>::move_only::copy_constructor_ok(), \
                      " - " #T " : must not declare copy constructor"); \
        static_assert(weftworks::common::utility::defensive<T>::move_only::move_constructor_ok(), \
                      " - " #T " : must declare move constructor"); \
        static_assert(weftworks::common::utility::defensive<T>::move_only::copy_assignment_ok(), \
                      " - " #T " : must not declare copy assignment operator"); \
        static_assert(weftworks::common::utility::defensive<T>::move_only::move_assignment_ok(), \
                      " - " #T " : must declare move assignment operator")

/// ensures your class is non constructible, aka had no default constructor,
/// destructor, copy constructor, move constructor, copy assignment operator and
/// move assignment operator. useful when writing static singleton classes
#define WEFTWORKS_DEFENSIVE_FULFILLS_NON_CONSTRUCTIBLE(T) \
        static_assert(weftworks::common::utility::defensive<T>::non_constructible::rule_pass(), \
                      #T " : doesn't fulfill non-constructible"); \
        static_assert(weftworks::common::utility::defensive<T>::non_constructible::constructor_ok(), \
                      " - " #T " : must not declare default constructor"); \
        static_assert(weftworks::common::utility::defensive<T>::non_constructible::destructor_ok(), \
                      " - " #T " : must not declare destructor"); \
        static_assert(weftworks::common::utility::defensive<T>::non_constructible::copy_constructor_ok(), \
                      " - " #T " : must not declare copy constructor"); \
        static_assert(weftworks::common::utility::defensive<T>::non_constructible::move_constructor_ok(), \
                      " - " #T " : must not declare move constructor"); \
        static_assert(weftworks::common::utility::defensive<T>::non_constructible::copy_assignment_ok(), \
                      " - " #T " : must not declare copy assignment operator"); \
        static_assert(weftworks::common::utility::defensive<T>::non_constructible::move_assignment_ok(), \
                      " - " #T " : must not declare move assignment operator")
