// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <memory>

#include <boost/asio.hpp>
#include <boost/logic/tribool.hpp>
#include <spdlog/spdlog.h>

#include "openflow/openflow.hpp"

#include "header.hpp"

namespace weftworks::common::network::openflow {
namespace implementation {

/// create and receive openflow connections
///
template<class T>
class connection
        : public network::tcp::connection<connection<T>>
{
public:
        /// constructor
        ///
        /// \param io_context is a reference to a boost asio io context
        explicit connection(boost::asio::io_context& io_context);

        /// start the data exchange
        ///
        auto start() -> void final;
protected:
        /// handle data read from the socket
        ///
        /// \param bytes_transferred is the amount of bytes read from the socket
        auto on_read(const size_t& bytes_transferred) -> void final;
private:
        std::shared_ptr<spdlog::logger> logger; ///< logger
        T request_handler;                      ///< connection request handler
};

// include implementation
#include "connection.tpp"

} // namespace implementation

// lift the implementation
template<class T>
using connection = implementation::connection<T>;

} // namespace weftworks::common::network::openflow
