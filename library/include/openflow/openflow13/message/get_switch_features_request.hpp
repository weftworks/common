// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <string>

#include <boost/logic/tribool.hpp>

#include "openflow/common.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {

namespace implementation {

/// present a get switch features request message
///
struct get_switch_features_request
{
        /// constructor
        ///
        get_switch_features_request();

        openflow::header header; ///< openflow header
};

} // namespace implementation

// lift the implementation
using get_switch_features_request = implementation::get_switch_features_request;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const get_switch_features_request& message) -> std::string;

/// parse payload from string
///
/// \param data is the data to parse
auto parse_payload(get_switch_features_request& message, std::string_view data) -> boost::tribool;

} // namespace weftworks::common::openflow::openflow13::message
