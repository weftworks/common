// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iomanip>
#include <sstream>
#include <string>

#include "openflow/common.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {

namespace implementation {

/// examine a captured tcp packet received from an openflow switch
///
struct packet_in
{
        /// constructor
        ///
        packet_in();

        openflow::header header;        ///< openflow header
        uint32_t buffer_id;             ///< id for tracking the captured packet
        uint16_t length;                ///< length of the captured packet
        uint8_t reason;                 ///< reason why the packet was captured
        uint8_t table_id;               ///< flow table id
        uint64_t cookie;                ///< flow entry cookie
        uint32_t match;                 ///< packet metadata
        uint16_t padding;               ///< data padding
        std::array<uint8_t, 6> data;    ///< the captured packet
};

} // namespace implementation

// lift the implementation
using packet_in = implementation::packet_in;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const packet_in& message) -> std::string;

} // namespace weftworks::common::openflow::openflow13::message
