// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <catch2/catch.hpp>

#include "network.hpp"

/// use for unit testing
///
class test_connection
        : public weftworks::common::network::tcp::connection<test_connection>
{
public:
        /// constructor
        ///
        /// \param io_context is a reference to a boost asio io context
        explicit test_connection(boost::asio::io_context& io_context)
                : weftworks::common::network::tcp::connection<test_connection>(io_context)
        { }
};

TEST_CASE("connection manager", "[require]")
{
        auto io_context = boost::asio::io_context{};
        auto manager = std::make_shared<weftworks::common::network::tcp::connection_manager<test_connection>>(io_context);

        SECTION("connection count on creation") {
                REQUIRE(manager->count() == 0);
        }
}
