// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

template<typename InputIterator>
auto inline request_parser::parse(InputIterator begin, InputIterator end) -> std::pair<boost::tribool, InputIterator>
{
        auto result = boost::tribool{boost::indeterminate};
        while (begin != end) {
                result = consume(*begin++);
                if (result or not result)
                        break;
        }
        return std::make_pair(result, begin);
}

auto inline request_parser::parse(std::string_view data) -> boost::tribool
{
        auto result = boost::tribool{boost::indeterminate};
        for (auto it : data) {
                result = consume(it);
                if (result or not result)
                        break;
        }
        return result;
}

auto inline request_parser::parse(char input) -> boost::tribool
{
        return consume(input);
}

auto inline request_parser::is_char(int c) const -> bool
{
        return c >= 0 and c <= 127;
}

auto inline request_parser::is_digit(int c) const -> bool
{
        return c >= '0' and c <= '9';
}
