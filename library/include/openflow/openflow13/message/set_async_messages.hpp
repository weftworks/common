// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <array>
#include <iomanip>
#include <sstream>
#include <string>

#include "openflow/common.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {

namespace implementation {

/// present a set async messages message
///
struct set_async_messages
{
        /// constructor
        ///
        set_async_messages();

        openflow::header header;                        ///< openflow header
        //std::array<uint32_t, 2> packet_in_mask;         ///< bitmask of packet_in reason fields
        //std::array<uint32_t, 2> port_status_mask;       ///< bitmask of port status reason fields
        //std::array<uint32_t, 2> flow_removed_mask;      ///< bitmask of flow removed reason fields
};

} // namespace implementation

// lift the implementation
using set_async_messages = implementation::set_async_messages;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const set_async_messages& message) -> std::string;

} // namespace weftworks::common::openflow::openflow13::message
