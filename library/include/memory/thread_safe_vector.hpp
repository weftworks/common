// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <mutex>
#include <utility>
#include <vector>

namespace weftworks::common::memory {

namespace implementation {

/// store objects in a vector
///
template<class T>
class thread_safe_vector
{
public:
        using iterator = typename std::vector<T>::iterator;                     ///< alias for an iterator
        using const_iterator = typename std::vector<T>::const_iterator;         ///< alias for a const iterator

        /// constructor
        ///
        thread_safe_vector();
        /// constructor
        ///
        /// \param size is the initial size of the vector
        thread_safe_vector(size_t size);
        /// copy assignment operator
        ///
        auto operator=(const thread_safe_vector<T>& other) -> thread_safe_vector<T>&;
        /// move assignment operator
        ///
        auto operator=(thread_safe_vector<T>&& other) -> thread_safe_vector<T>&;
        /// get the element at a position
        ///
        /// \return a reference to the element
        auto at(size_t position) -> T&;
        /// get an iterator to the beginning
        ///
        auto begin() -> const_iterator;
        /// get an iterator to the beginning
        ///
        auto end() -> const_iterator;
        /// get the capacity of the vector
        ///
        /// \return the capacity
        auto capacity() const -> size_t;
        /// get the number of elements in the vector
        ///
        /// \return the element count
        auto size() const -> size_t;
        /// remove all elements from the vector
        ///
        auto clear() -> void;
        /// erase an element from the vector
        ///
        /// \param position is an element position
        /// \return an iterator to the next position after erasure
        auto erase(iterator position) -> iterator;
        /// erase elements from the vector in range
        ///
        /// \param first is the starting position
        /// \param last is the ending position
        /// \return an iterator to the next position after erasure
        auto erase(iterator first, iterator last) -> iterator;
        /// erase an element from the vector
        ///
        /// \param position is an element position
        /// \return an iterator to the next position after erasure
        auto erase(const_iterator position) -> iterator;
        /// erase elements from the vector in range
        ///
        /// \param first is the starting position
        /// \param last is the ending position
        /// \return an iterator to the next position after erasure
        auto erase(const_iterator first, const_iterator last) -> iterator;
        /// insert an element at the end of the vector
        ///
        template<class... Args>
        auto emplace_back(Args&&... args) -> void;
        /// remove the last element from the vector
        ///
        auto pop_back() -> void;
        /// increate the capacity of the vector
        ///
        auto reserve(size_t size) -> void;
        /// resize the vector
        ///
        auto resize(size_t size) -> void;
private:
        std::vector<T> vector;          ///< the underlying vector
        mutable std::mutex mutex;       ///< a mutable mutex
};


template<class T>
inline thread_safe_vector<T>::thread_safe_vector()
        : vector{}
{ }

template<class T>
inline thread_safe_vector<T>::thread_safe_vector(size_t size)
        : vector{}
{
        resize(size);
}

template<class T>
auto inline thread_safe_vector<T>::operator=(const thread_safe_vector<T>& other) -> thread_safe_vector<T>&
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        vector = other;
        return *this;
}

template<class T>
auto inline thread_safe_vector<T>::operator=(thread_safe_vector<T>&& other) -> thread_safe_vector<T>&
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        vector = std::move(other);
        return *this;
}

template<class T>
auto inline thread_safe_vector<T>::at(size_t position) -> T&
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return vector.at(position);
}

template<class T>
auto inline thread_safe_vector<T>::begin() -> const_iterator
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return vector.begin();
}

template<class T>
auto inline thread_safe_vector<T>::end() -> const_iterator
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return vector.end();
}

template<class T>
auto inline thread_safe_vector<T>::capacity() const -> size_t
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return vector.capacity();
}

template<class T>
auto inline thread_safe_vector<T>::size() const -> size_t
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return vector.size();
}

template<class T>
auto inline thread_safe_vector<T>::clear() -> void
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        vector.clear();
}

template<class T>
auto inline thread_safe_vector<T>::erase(iterator position) -> iterator
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return vector.erase(position);
}

template<class T>
auto inline thread_safe_vector<T>::erase(iterator first, iterator last) -> iterator
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return vector.erase(first, last);
}

template<class T>
auto inline thread_safe_vector<T>::erase(const_iterator position) -> iterator
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return vector.erase(position);
}

template<class T>
auto inline thread_safe_vector<T>::erase(const_iterator first, const_iterator last) -> iterator
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        return vector.erase(first, last);
}

template<class T>
template<class... Args>
auto inline thread_safe_vector<T>::emplace_back(Args&&... args) -> void
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        vector.emplace_back(std::forward<T>(args)...);
}

template<class T>
auto inline thread_safe_vector<T>::pop_back() -> void
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        vector.pop_back();
}

template<class T>
auto inline thread_safe_vector<T>::reserve(size_t size) -> void
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        vector.reserve(size);
}

template<class T>
auto inline thread_safe_vector<T>::resize(size_t size) -> void
{
        auto guard = std::lock_guard<std::mutex>{mutex};
        vector.resize(size);
}


} // namespace implementation

// lift the implementation
template<class T>
using thread_safe_vector = implementation::thread_safe_vector<T>;

} // namespace weftworks::common::memory
