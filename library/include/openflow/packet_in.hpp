// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

namespace weftworks::common::openflow::packet_in {

/// the reason why a tcp packet was captured
///
enum class reason : uint8_t
{
        no_match        = 0x00, ///< flow table match miss
        action          = 0x01, ///< explicitly send the packet to a controller
        invalid_ttl     = 0x02, ///< the packet has an invalid time-to-live value
};

} // namespace weftworks::common::openflow::packet_in
