# Weftworks Common Library

[![pipeline status](https://gitlab.com/weftworks/common/badges/staging/pipeline.svg)](https://gitlab.com/weftworks/common/commits/staging)
[![coverage report](https://gitlab.com/weftworks/common/badges/staging/coverage.svg)](https://gitlab.com/weftworks/common/commits/staging)

## Description

A common C++ library used by the controller and the test bench.
