// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iomanip>
#include <sstream>
#include <string>

#include "openflow/common.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {

namespace implementation {

/// present a flow entry removed message
///
struct flow_removed
{
        /// constructor
        ///
        flow_removed();

        openflow::header header;        ///< openflow header
        uint64_t cookie;                ///< flow entry identifier given by a controller
        uint16_t priority;              ///< flow entry priority
        uint8_t reason;                 ///< removal reason
        uint8_t table_id;               ///< table id
        uint32_t duration_seconds;      ///< flow entry duration in seconds
        uint32_t duration_nanoseconds;  ///< flow entry duration beyond duration_seconds in nanoseconds
        uint16_t idle_timeout;          ///< original flow entry modification idle timeout
        uint16_t hard_timeout;          ///< original flow entry modification hard timeout
        uint64_t packet_count;          ///< number of packets tracked
        uint64_t byte_count;            ///< number of bytes tracked
        uint32_t match;                 ///< flow entry match fields
};

} // namespace implementation

// lift the implementation
using flow_removed = implementation::flow_removed;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const flow_removed& message) -> std::string;

} // namespace weftworks::common::openflow::openflow13::message
