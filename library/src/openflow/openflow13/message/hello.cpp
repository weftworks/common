// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "openflow/openflow13/message/hello.hpp"

namespace weftworks::common::openflow::openflow13::message {
namespace implementation {

hello::hello()
        : header{0, 0, 0, 0}
        , elements{}
{
        auto constexpr message_default_length = uint_fast8_t{8};

        header.version = +openflow::version::openflow13;
        header.type = +openflow::message::hello;
        header.length = message_default_length;
}

auto serialize_hello_element(const openflow::hello::version_bitmap& element) -> std::string
{
        auto data = std::string{};

        data.push_back(element.type >> 8);
        data.push_back(element.type);
        data.push_back(element.length >> 8);
        data.push_back(element.length);

        for (auto bitmap : element.bitmaps) {
                data.push_back(bitmap >> 24);
                data.push_back(bitmap >> 16);
                data.push_back(bitmap >> 8);
                data.push_back(bitmap);
        }

        return data;
}

} // namespace implementation

auto serialize_payload(const hello& message) -> std::string
{
        auto data = std::string{};

        // serialize hello elements
        for (auto element : message.elements) {
                data += implementation::serialize_hello_element(element);
        }

        return data;
}

auto parse_payload(hello& message, std::string_view data) -> boost::tribool
{
        auto result = boost::tribool{boost::indeterminate};

        auto constexpr hello_element_length = uint_fast8_t{8};
        auto constexpr data_min_length = uint_fast8_t{hello_element_length};

        if (data.length() < data_min_length)
                return false;

        auto it = data.begin();

        do {
                auto payload = common::openflow::hello::version_bitmap{};

                payload.type |= ((uint8_t)*it << 8);     ++it;
                payload.type |= (uint8_t)*it;            ++it;
                payload.length |= ((uint8_t)*it << 8);   ++it;
                payload.length |= (uint8_t)*it;          ++it;

                auto bitmap = uint32_t{0};

                bitmap |= (uint8_t)*it << 24;    ++it;
                bitmap |= (uint8_t)*it << 16;    ++it;
                bitmap |= (uint8_t)*it << 8;     ++it;
                bitmap |= (uint8_t)*it;          ++it;

                payload.bitmaps.emplace_back(bitmap);
                message.elements.emplace_back(payload);
        } while (std::distance(it, data.end()) > hello_element_length);

        return result;
}

} // namespace weftworks::common::openflow::openflow13::message
