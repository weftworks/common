// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

namespace weftworks::common::openflow::openflow_switch {

/// openflow switch capabilities
///
enum class capabilities : uint32_t
{
        flow_statistics         = 0x00000001, ///< the switch can report flow statistics
        table_statistics        = 0x00000002, ///< the switch can report table statistics
        port_statistics         = 0x00000004, ///< the switch can report port statistics
        group_statistics        = 0x00000008, ///< the switch can report group statistics
        ip_reassembly           = 0x00000020, ///< the switch can reassemble ip fragments
        queue_statistics        = 0x00000040, ///< the switch can report queue statistics
        port_blocked            = 0x00000100, ///< the switch can prevent packet loops
};

} // namespace weftworks::common::openflow::switch
