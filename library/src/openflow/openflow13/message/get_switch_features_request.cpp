// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "openflow/openflow13/message/get_switch_features_request.hpp"

namespace weftworks::common::openflow::openflow13::message {
namespace implementation {

get_switch_features_request::get_switch_features_request()
        : header{0, 0, 0, 0}
{
        auto constexpr message_default_length = uint_fast8_t{8};

        header.version = +openflow::version::openflow13;
        header.type = +openflow::message::get_switch_features_request;
        header.length = message_default_length;
}

} // namespace implementation

auto serialize_payload(const get_switch_features_request& message) -> std::string
{
        auto data = std::string{};

        return data;
}

auto parse_payload(get_switch_features_request& message, std::string_view data) -> boost::tribool
{
        return true;
}

} // namespace weftworks::common::openflow::openflow13::message
