// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

template<typename T>
inline message::message(const T& message)
        : parser_state(state::parse_header)
{
        logger = spdlog::get("openflow message");
        if (not logger)
                logger = spdlog::default_logger()->clone("openflow message");

        msg = std::make_shared<message_model<T>>(message);
}

inline message::message()
        : parser_state(state::parse_header)
{
        logger = spdlog::get("openflow message");
        if (not logger)
                logger = spdlog::default_logger()->clone("openflow message");
}

auto inline message::get_data() const -> std::optional<std::string>
{
        return msg ? msg->get_data() : std::nullopt;
}

auto inline message::get_header() const -> std::optional<common::openflow::header>
{
        return msg ? msg->get_header() : std::nullopt;
}

auto inline message::get_length() const -> int
{
        return msg ? msg->get_length() : 0;
}

auto inline message::get_message() const -> std::shared_ptr<void>
{
        return msg ? msg->get_message() : nullptr;
}

auto inline message::get_name() const -> std::optional<std::string>
{
        return msg ? msg->get_name() : std::nullopt;
}

auto inline message::get_type() const -> std::optional<common::openflow::message>
{
        return msg ? msg->get_type() : std::nullopt;
}

auto inline message::get_version() const -> std::optional<common::openflow::version>
{
        return msg ? msg->get_version() : std::nullopt;
}

auto inline message::parse(std::string_view data) -> boost::tribool
{
        auto result = boost::tribool{boost::indeterminate};

        auto header = network::openflow::header{};
        auto [header_result, begin] = header.parse(data.begin(), data.end());

        if (not header_result) {
                return false;
        }

        auto remaining_data = std::string{begin, data.end()};
        logger->trace("Remaining data length: {}.", remaining_data.length());

        // TODO: fix this mess
        switch(header.get_message_type()) {
        case +common::openflow::message::hello: {
                auto hello = common::openflow::openflow13::message::hello{};
                auto payload_result = common::openflow::openflow13::message::parse_payload(hello, remaining_data);
                for (auto element : hello.elements) {
                        logger->trace("Parsed Hello element, type: {}, length: {}", element.type, element.length);
                        for (auto bitmap : element.bitmaps) {
                                logger->trace("Bitmap version: {}", bitmap);
                        }
                }
                msg = std::make_shared<message_model<common::openflow::openflow13::message::hello>>(hello);
                break;
        }
        case +common::openflow::message::get_switch_features_request: {
                auto get_switch_features_request = common::openflow::openflow13::message::get_switch_features_request{};

                msg = std::make_shared<message_model<common::openflow::openflow13::message::get_switch_features_request>>(get_switch_features_request);
                break;
        }
        default: {
                return false;
        }
        }

        logger->debug("Parsed a OpenFlow {} message.", get_name().value());

        return result;
}

auto inline message::parse_header(std::string_view data) -> boost::tribool
{
        auto result = boost::tribool{boost::indeterminate};

        auto header = network::openflow::header{};
        auto [header_result, begin] = header.parse(data.begin(), data.end());

        if (not header_result) {
                logger->debug("Couldn't parse a header.");
                return false;
        }

        // TODO: fix this mess
        switch(header.get_message_type()) {
        case +common::openflow::message::hello: {
                auto hello = common::openflow::openflow13::message::hello{};
                hello.header = header.get_header();
                msg = std::make_shared<message_model<common::openflow::openflow13::message::hello>>(hello);
                break;
        }
        case +common::openflow::message::error: {
                auto error = common::openflow::openflow13::message::error{};
                error.header = header.get_header();
                msg = std::make_shared<message_model<common::openflow::openflow13::message::error>>(error);
                break;
        }
        case +common::openflow::message::get_switch_features_request: {
                auto get_switch_features_request = common::openflow::openflow13::message::get_switch_features_request{};
                get_switch_features_request.header = header.get_header();
                msg = std::make_shared<message_model<common::openflow::openflow13::message::get_switch_features_request>>(get_switch_features_request);
                break;
        }
        default: {
                logger->debug("Unimplemented message type in header: {}.", header.get_message_type());
                return false;
        }
        }

        logger->debug("Parsed a OpenFlow {} message header.", get_name().value());
        logger->debug("Header length: {}.", header.get_length());

        return result;
}

auto inline message::parse_payload(std::string_view data) -> boost::tribool
{
        if (not msg) {
                logger->error("Attempted to parse a payload before a header!");
                return false;
        }

        auto result = boost::tribool{boost::indeterminate};

        // TODO: refactor this mess
        switch(get_header().value().type) {
        case +common::openflow::message::hello: {
                auto _msg = std::static_pointer_cast<common::openflow::openflow13::message::hello>(get_message());
                result = common::openflow::openflow13::message::parse_payload(*_msg, data);
                break;
        }
        case +common::openflow::message::error: {
                auto _msg = std::static_pointer_cast<common::openflow::openflow13::message::error>(get_message());
                result = common::openflow::openflow13::message::parse_payload(*_msg, data);
                break;
        }
        case +common::openflow::message::get_switch_features_request: {
                auto _msg = std::static_pointer_cast<common::openflow::openflow13::message::get_switch_features_request>(get_message());
                result = common::openflow::openflow13::message::parse_payload(*_msg, data);
                break;
        }
        default: {
                return false;
        }
        }

        if (result)
                logger->debug("Parsed {} message's payload.", get_name().value());

        return result;
}

inline message::message_concept::~message_concept()
{ }

template<typename T>
inline message::message_model<T>::message_model(const T& message)
        : message{message}
{
        logger = spdlog::get("openflow message");
        if (not logger)
                logger = spdlog::default_logger()->clone("openflow message");
}

template<typename T>
inline message::message_model<T>::~message_model()
{ }

template<typename T>
auto inline message::message_model<T>::get_data() const -> std::optional<std::string>
{
        if (not is_valid_header(message.header))
                return std::nullopt;

        auto data = std::string{};

        logger->debug("Get data header: {}, {}, {}, {}.", message.header.version, message.header.type, message.header.length, message.header.xid);

        // serialize header
        data.push_back(message.header.version);
        data.push_back(message.header.type);
        data.push_back(message.header.length >> 8);
        data.push_back(message.header.length);
        data.push_back(message.header.xid >> 24);
        data.push_back(message.header.xid >> 16);
        data.push_back(message.header.xid >> 8);
        data.push_back(message.header.xid);

        // serialize message specific payload
        switch(get_version().value()) {
        case common::openflow::version::openflow10:
                // TODO: implement. this case exists for Test Bench's errors test.
                break;
        case common::openflow::version::openflow13:
                data += common::openflow::openflow13::message::serialize_payload(message);
                break;
        default:
                return std::nullopt;
        }

        return data;
}

template<typename T>
auto inline message::message_model<T>::get_header() const -> std::optional<common::openflow::header>
{
        return message.header;
}

template<typename T>
auto inline message::message_model<T>::get_length() const -> int
{
        return message.header.length;
}

template<typename T>
auto inline message::message_model<T>::get_message() const -> std::shared_ptr<void>
{
        return std::make_shared<T>(message);
}

template<typename T>
auto inline message::message_model<T>::get_name() const -> std::optional<std::string>
{
        if (not get_type())
                return std::nullopt;

        switch(get_type().value()) {
        case common::openflow::message::hello:
                return "Hello";
        case common::openflow::message::error:
                return "Error";
        case common::openflow::message::echo_request:
                return "EchoRequest";
        case common::openflow::message::echo_response:
                return "EchoResponse";
        case common::openflow::message::experimenter:
                return "Experimenter";
        case common::openflow::message::get_switch_features_request:
                return "FeatureReq";
        case common::openflow::message::get_switch_features_response:
                return "FeatureRes";
        case common::openflow::message::get_switch_config_request:
                return "GetConfigReq";
        case common::openflow::message::get_switch_config_response:
                return "GetConfigRes";
        case common::openflow::message::set_switch_config:
                return "SetConfig";
        case common::openflow::message::packet_in:
                return "PacketIn";
        case common::openflow::message::flow_removed:
                return "FlowRemoved";
        case common::openflow::message::port_status:
                return "PortStatus";
        case common::openflow::message::packet_out:
                return "PacketOut";
        case common::openflow::message::flow_modification:
                return "FlowMod";
        case common::openflow::message::group_modification:
                return "GroupMod";
        case common::openflow::message::port_modification:
                return "PortMod";
        case common::openflow::message::table_modification:
                return "TableMod";
        case common::openflow::message::multipart_request:
                return "MultipartReq";
        case common::openflow::message::multipart_response:
                return "MultipartRes";
        case common::openflow::message::barrier_request:
                return "BarrierReq";
        case common::openflow::message::barrier_response:
                return "BarrierRes";
        case common::openflow::message::get_queue_config_request:
                return "QueueGetConfigReq";
        case common::openflow::message::get_queue_config_response:
                return "QueueGetConfigRes";
        case common::openflow::message::controller_role_request:
                return "RoleReq";
        case common::openflow::message::controller_role_response:
                return "RoleRes";
        case common::openflow::message::get_async_messages_request:
                return "GetAsyncReq";
        case common::openflow::message::get_async_messages_response:
                return "GetAsyncRes";
        case common::openflow::message::set_async_messages:
                return "SetSync";
        case common::openflow::message::meter_modification:
                return "MeterMod";
        case common::openflow::message::controller_role_status:
                return "RoleStatus";
        case common::openflow::message::table_status:
                return "TableStatus";
        case common::openflow::message::request_forward:
                return "RequestForward";
        case common::openflow::message::bundle_control:
                return "BundleControl";
        case common::openflow::message::bundle_add_message:
                return "BundleAddMessage";
        default:
                return std::nullopt;
        }
}

template<typename T>
auto inline message::message_model<T>::get_type() const -> std::optional<common::openflow::message>
{
        if (is_valid_type(message.header.type))
                return static_cast<common::openflow::message>(message.header.type);
        else
                return std::nullopt;
}

template<typename T>
auto inline message::message_model<T>::get_version() const -> std::optional<common::openflow::version>
{
        if (is_valid_version(message.header.version))
                return static_cast<common::openflow::version>(message.header.version);
        else
                return std::nullopt;
}

template<typename T>
auto inline message::message_model<T>::parse_header(char input) -> boost::tribool
{
        // auto result = boost::tribool{false};
        // // parse header
        // std::tie(result, std::ignore) = header.parse(data.begin(), data.end());
        // if (result)
        //         logger->trace("Valid OpenFlow header received.");
        // else if (not result)
        //         logger->trace("Invalid OpenFlow header received.");
        // else
        //         logger->trace("Indetermined.");
        // return result;
        return false;
        //return header.parse(input);
}

auto inline message::consume(char input) -> boost::tribool
{
        logger->trace("Parser state: {}.", (int)parser_state);
        if (is_char(input)) {
                switch(parser_state) {
                case state::parse_header:
                        return parse_header(input);
                default:
                        break;
                }
        }
        return false;
}

auto inline message::parse_header(char input) -> boost::tribool
{
        if (msg)
                return msg->parse_header(input);
        else
                return false;
}

auto inline message::reset_parser_state() -> void
{
        parser_state = state::parse_header;
}

auto inline is_valid_header(const common::openflow::header& header) -> bool
{
        return is_valid_type(header.type) and is_valid_version(header.version);
}

auto inline is_valid_type(uint8_t type) -> bool
{
        return type >= +common::openflow::message::hello and type <= +common::openflow::message::max_value;
}

auto inline is_valid_version(uint8_t version) -> bool
{
        return version >= +common::openflow::version::openflow10 and version <= +common::openflow::version::latest;
}
