// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <array>

#include <iomanip>
#include <sstream>
#include <string>

#include "openflow/common.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {

namespace implementation {

/// present a get async messages request message
///
struct get_async_messages_response
{
        /// constructor
        ///
        get_async_messages_response();

        openflow::header header;                        ///< openflow header
};

} // namespace implementation

// lift the implementation
using get_async_messages_response = implementation::get_async_messages_response;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const get_async_messages_response& message) -> std::string;

} // namespace weftworks::common::openflow::openflow13::message
