// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "openflow/header.hpp"

namespace weftworks::common::openflow {
namespace implementation {

header::header()
        : version{0}
        , type{0}
        , length{0}
        , xid{0}
{ }

header::header(uint8_t version_, uint8_t type_, uint16_t length_, uint32_t xid_)
        : version{version_}
        , type{type_}
        , length{length_}
        , xid{xid_}
{ }

} // namespace implementation
} // namespace weftworks::common::openflow::action
