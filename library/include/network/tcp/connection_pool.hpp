// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <memory>
#include <optional>
#include <type_traits>

#include <boost/asio/io_context.hpp>

#include "memory.hpp"
#include "utility.hpp"

namespace weftworks::common::network::tcp {
namespace implementation {

/// provide a pool of tcp connections
///
template<class T>
class connection_pool
        : public std::enable_shared_from_this<connection_pool<T>>
        , public utility::noncopyable
{
private:
        /// return a connection automatically back to the pool after its destruction
        ///
        class connection_deleter {
        public:
                /// constructor
                ///
                /// \param pool is a weak reference to a connection pool
                explicit connection_deleter(std::shared_ptr<connection_pool<T>> pool);

                /// return a connection back to the pool or delete it
                ///
                /// \param connection is a raw pointer to a connection
                auto operator()(T* connection) -> void;
        private:
                std::weak_ptr<connection_pool<T>> pool; ///< weak pointer to a connection pool
        };
public:
        using managed_connection_ptr = std::unique_ptr<T, connection_deleter>;  ///< connection managed by a connection pool typedef

        //// constructor
        ///
        /// \param io_context is a reference to a boost asio io context
        /// \param init_size is the initial pool size
        /// \param max_size is the maximum pool size
        connection_pool(boost::asio::io_context& io_context, const size_t& init_size, const size_t& max_size);

        /// get a connection from the pool if one is available
        ///
        /// \return a unique pointer to a connection
        auto get() -> std::optional<managed_connection_ptr>;

        /// check if the pool is empty
        ///
        /// \return a boolean value
        auto empty() const -> bool;

        /// get the number of available connections
        ///
        /// \return the available connection count
        auto free() const -> size_t;

        /// get the maximum pool size
        ///
        /// \return the maximum pool size
        auto max() const -> size_t;

        /// get the current size of the pool
        ///
        /// \return the current size of the pool
        auto size() const -> size_t;
private:
        /// add a connection to the pool
        ///
        /// \param connection is a unique pointer to a connection
        auto add(std::unique_ptr<T> connection = nullptr) -> void;

        /// create a set amount of connections
        ///
        /// \param size is the number of connections to create
        auto initialize(const size_t& size) -> void;

        /// noop on return event
        ///
        auto virtual on_return() -> void;

        boost::asio::io_context& io_context;                    ///< reference to a boost asio io context
        size_t available;                                       ///< number of available connections
        size_t max_size;                                        ///< maximum pool size
        size_t pool_size;                                       ///< current pool size
        memory::thread_safe_stack<std::unique_ptr<T>> pool;     ///< stored connections
};

// include implementation
#include "connection_pool.tpp"

} // namespace implementation

// lift the implementation
template<class T>
using connection_pool = implementation::connection_pool<T>;

template<class T>
using managed_connection_ptr = typename implementation::connection_pool<T>::managed_connection_ptr;

} // namespace weftworks::common::network::tcp
