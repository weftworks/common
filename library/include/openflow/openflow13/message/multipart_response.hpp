// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iomanip>
#include <sstream>
#include <string>

#include "openflow/common.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {

namespace implementation {

/// present a multipart request
///
struct multipart_response
{
        /// constructor
        ///
        multipart_response();

        openflow::header header;        ///< openflow header
        uint16_t type;                  ///< request type
        uint16_t flags;                 ///< request flags
        uint32_t padding;               ///< data padding
};

} // namespace implementation

// lift the implementation
using multipart_response = implementation::multipart_response;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const multipart_response& message) -> std::string;

} // namespace weftworks::common::openflow::openflow13::message
