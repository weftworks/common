// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <string>

#include <boost/logic/tribool.hpp>

#include "openflow/common.hpp"
#include "openflow/header.hpp"
#include "openflow/openflow_switch.hpp"

namespace weftworks::common::openflow::openflow13::message {

namespace implementation {

/// present a get switch features response message
///
struct get_switch_features_response
{
        /// constructor
        ///
        get_switch_features_response();

        openflow::header header;        ///< openflow header
        uint64_t datapath_id;           ///< unique datapath id
        uint32_t n_buffers;             ///< number of packet_in messages the switch can queue
        uint8_t n_tables;               ///< number of tables in the switch
        uint8_t auxiliary_id;           ///< how the switch treats the transport channel (master/auxiliary)
        uint16_t padding;               ///< data padding
        uint32_t capabilities;          ///< bitmap of switch capabilities
        uint32_t reserved;              ///< reserved bits
};

} // namespace implementation

// lift the implementation
using get_switch_features_response = implementation::get_switch_features_response;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const get_switch_features_response& message) -> std::string;

} // namespace weftworks::common::openflow::openflow13::message
