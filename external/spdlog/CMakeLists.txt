# ./external/spdlog/CMakeLists.txt

find_package(Git REQUIRED)

# spdlog version
set(SPDLOG_VERSION 1.3.1)

# Add external project
include(ExternalProject)

ExternalProject_Add(
        spdlog
        CMAKE_ARGS              "-DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/external/spdlog"
        GIT_REPOSITORY          https://github.com/gabime/spdlog.git
        GIT_SHALLOW             1
        GIT_TAG                 "v${SPDLOG_VERSION}"
        CONFIGURE_COMMAND       ""
        BUILD_COMMAND           ""
        INSTALL_COMMAND         ""
        BINARY_DIR              ${CMAKE_BINARY_DIR}/external/spdlog/build
        DOWNLOAD_DIR            ${CMAKE_BINARY_DIR}/external/spdlog/download
        PREFIX                  ${CMAKE_BINARY_DIR}/external/spdlog/prefix
        STAMP_DIR               ${CMAKE_BINARY_DIR}/external/spdlog/stamp
        SOURCE_DIR              ${CMAKE_BINARY_DIR}/external/spdlog/src
        TMP_DIR                 ${CMAKE_BINARY_DIR}/external/spdlog/tmp
)
