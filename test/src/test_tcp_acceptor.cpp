// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <catch2/catch.hpp>

#include <network/tcp/acceptor.hpp>
#include <test/network/tcp/connection.hpp>

namespace tcp = weftworks::common::network::tcp;
namespace test = weftworks::common::test::network::tcp;

TEST_CASE("tcp acceptor", "[require]")
{
        auto io_context = boost::asio::io_context{};
        auto acceptor = tcp::acceptor<test::connection>{io_context};

        const auto address = std::string{"127.0.0.1"};
        auto port = uint16_t{0}; // using 0 binds the acceptor to any free port

        SECTION("should be able to enter listening state") {
                REQUIRE(acceptor.bind(address, port) == true);
                REQUIRE(acceptor.is_open() == true);
        }

        SECTION("should be able to leave listening state") {
                acceptor.bind(address, port);
                REQUIRE(acceptor.close() == true);
                REQUIRE(acceptor.is_open() == false);
        }

        SECTION("should be able to rebind to an address after a quick restart") {
                acceptor.bind(address, port);
                acceptor.close();
                REQUIRE(acceptor.bind(address, port) == true);
        }

        SECTION("should return the port number currently bound to") {
                // we shouldn't use static ports so bind once to get
                // a free port, then use that port number for the test
                REQUIRE(acceptor.get_port() == 0); // should return 0 when not bound
                acceptor.bind(address, port);
                port = acceptor.get_port();
                acceptor.close();
                acceptor.bind(address, port);
                REQUIRE(acceptor.get_port() == port);
        }
}
