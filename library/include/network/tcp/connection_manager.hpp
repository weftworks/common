// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <algorithm>
#include <vector>

#include "memory.hpp"
#include "utility.hpp"

namespace weftworks::common::network::tcp {
namespace implementation {

/// manage active connections
///
template<class T, template<class...> typename C = std::shared_ptr>
class connection_manager
{
public:
        /// constructor
        ///
        /// \param io_context is a reference to a boost asio io context
        explicit connection_manager(boost::asio::io_context& io_context);

        /// get the current connection count
        ///
        auto count() -> size_t;

        /// add a connection to the manager
        ///
        /// \param connection is a unique pointer with a custom deleter to a connection
        auto add(C<T> connection) -> void;

        /// close and remove all active connections
        ///
        auto close_connections() -> void;
private:
        /// check for closed connections and remove them
        ///
        /// \param error is a boost error code
        auto do_cleanup(const boost::system::error_code& error) -> void;

        /// run do_cleanup after delay
        ///
        auto run_cleanup(const uint32_t seconds) -> void;

        boost::asio::deadline_timer cleanup_timer;      ///< boost asio timer for cleaning closed connections
        std::vector<C<T>> connections;                  ///< stored connections
};

// include implementation
#include "connection_manager.tpp"

} // namespace implementation

// lift the implementation
template<class T, template<class...> typename C = std::shared_ptr>
using connection_manager = implementation::connection_manager<T, C>;

} // namespace weftworks::common::network::tcp
