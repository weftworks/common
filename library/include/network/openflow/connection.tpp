// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


template<class T>
inline connection<T>::connection(boost::asio::io_context& io_context)
        : network::tcp::connection<connection>(io_context)
        , request_handler{}
{
        logger = spdlog::get("openflow");
        if (not logger)
                logger = spdlog::default_logger()->clone("openflow");
        logger->trace("New connection created.");
}

template<class T>
auto inline connection<T>::start() -> void
{
        logger->debug("New connection started.");
        this->set_timeout(120);
        this->async_read(8);
}

template<class T>
auto inline connection<T>::on_read(const size_t& bytes_transferred) -> void
{
        logger->trace("OpenFlow connection on read bytes: {}:{}", bytes_transferred, this->get_read_data().length());
        auto result = request_handler.handle_request(this->get_read_data());
        if (result) {
                logger->debug("Valid OpenFlow request received.");
                this->close();
        } else if (not result) {
                logger->debug("Invalid OpenFlow request received.");
                this->close();
        } else {
                logger->debug("Conn: Indetermined.");
                this->async_read(1);
        }
}
