// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "memory/thread_safe_vector.hpp"

// Credits to the Catch2 documentation for these vector tests.
// https://github.com/catchorg/Catch2/blob/master/docs/test-cases-and-sections.md
//
TEMPLATE_TEST_CASE("thread-safe vector", "[template]", int, std::string)
{
        auto vector = weftworks::common::memory::thread_safe_vector<TestType>{5};
        REQUIRE(vector.size() == 5);

        SECTION("resizing bigger changes size and capacity")
        {
                vector.resize(10);
                REQUIRE(vector.size() == 10);
                REQUIRE(vector.capacity() >= 10);
        }

        SECTION("resizing smaller changes size but not capacity")
        {
                vector.resize(0);
                REQUIRE(vector.size() == 0);
                REQUIRE(vector.capacity() >= 5);
        }

        SECTION("reserving smaller does not change size or capacity")
        {
                vector.reserve(0);
                REQUIRE(vector.size() == 5);
                REQUIRE(vector.capacity() >= 5);
        }
}
