// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <cstdint>

namespace weftworks::common::openflow::action {

/// action flags used by flow entries, groups and packets
///
enum class type : uint16_t
{
        output                  = 0x0000, ///< output to a switch port
        copy_ttl_out            = 0x000b, ///< copy a ttl outwards, from next outmost to outmost
        copy_ttl_in             = 0x000c, ///< copy a ttl inwards, from outmost to next outmost
        set_mpls_ttl            = 0x000f, ///< set a mpls ttl
        decrement_mpls_ttl      = 0x0010, ///< decrement a mpls ttl
        push_vlan_tag           = 0x0011, ///< push a new vlan tag
        pop_vlan_tag            = 0x0012, ///< pop a vlan tag
        push_mpls_tag           = 0x0013, ///< push a new mpls tag
        pop_mpls_tag            = 0x0014, ///< pop a mpls tag
        set_queue_id            = 0x0015, ///< set queue id
        apply_group             = 0x0016, ///< apply group
        set_ip_ttl              = 0x0017, ///< set an ip ttl
        decrement_ip_ttl        = 0x0018, ///< decrement an ip ttl
        set_header_field        = 0x0019, ///< set a header field
        push_pbb_tag            = 0x001a, ///< push a new provider backbone bridge service tag
        pop_pbb_tag             = 0x001b, ///< pop a provider backbone bridge service tag
        experimenter            = 0xffff, ///< experimenter defined
};

namespace implementation {

/// common header for all openflow actions
///
struct header
{
        /// constructor
        ///
        header();

        uint16_t type;          ///< action type
        uint16_t length;        ///< length of the action
        uint32_t padding;       ///< data padding
};

} // namespace implementation

// lift the implementation
using header = implementation::header;

} // namespace weftworks::common::openflow::action
