// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <string>
#include <string_view>

#include <boost/logic/tribool.hpp>

#include "openflow/common.hpp"
#include "openflow/error.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {
namespace implementation {

/// present openflow error messages
///
struct error
{
        /// constructor
        ///
        error();

        openflow::header header;        ///< openflow header
        uint16_t type;                  ///< high level error type
        uint16_t code;                  ///< type specific error code
        std::string data;               ///< arbitrary error message string
};

} // namespace implementation

// lift the implementation
using error = implementation::error;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const error& message) -> std::string;

/// parse payload from string
///
/// \param data is the data to parse
auto parse_payload(error& message, std::string_view data) -> boost::tribool;

} // namespace weftworks::common::openflow::openflow13::message
