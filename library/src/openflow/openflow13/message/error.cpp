// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "openflow/openflow13/message/error.hpp"

namespace weftworks::common::openflow::openflow13::message {
namespace implementation {

error::error()
        : header{0, 0, 0, 0}
        , type{0}
        , code{0}
        , data{}
{
        auto constexpr message_default_length = uint_fast8_t{12};

        header.version = +openflow::version::openflow13;
        header.type = +openflow::message::error;
        header.length = message_default_length;
}

} // namespace implementation

auto serialize_payload(const error& message) -> std::string
{
        auto data = std::string{};

        return data;
}

auto parse_payload(error& message, std::string_view data) -> boost::tribool
{
        auto constexpr error_min_length = uint_fast8_t{32};
        auto result = boost::tribool{boost::indeterminate};

        if (data.length() < error_min_length) {
                return false;
        }

        auto it = data.begin();

        message.type |= ((uint8_t)*it << 8);    ++it;
        message.type |= (uint8_t)*it;           ++it;

        message.code |= ((uint8_t)*it << 8);    ++it;
        message.code |= (uint8_t)*it;           ++it;

        message.data = std::string{it, data.end()};

        return result;
}


} // namespace weftworks::common::openflow::openflow13::message
