// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "openflow/openflow13/message/flow_modification.hpp"

namespace weftworks::common::openflow::openflow13::message {
namespace implementation {

flow_modification::flow_modification()
        : header{0, 0, 0, 0}
        , cookie{0}
        , cookie_mask{0}
        , table_id{0}
        , command{0}
        , idle_timeout{0}
        , hard_timeout{0}
        , priority{0}
        , buffer_id{0}
        , out_port{0}
        , out_group{0}
        , flags{0}
        , padding{0}
        , match{0}
        , instruction{0}
{
        header.version = +openflow::version::openflow13;
        header.type = +openflow::message::flow_modification;
}

} // namespace implementation

auto serialize_payload(const flow_modification& message) -> std::string
{
        auto data = std::ostringstream{};

        return data.str();
}

} // namespace weftworks::common::openflow::openflow13::message
