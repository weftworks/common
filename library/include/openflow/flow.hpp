// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

namespace weftworks::common::openflow::flow {

/// flow modification commands
///
enum class command : uint8_t
{
        add             = 0x00, ///< add a new flow entry
        modify          = 0x01, ///< modify matching flow entries
        modify_strict   = 0x03, ///< modify a strictly matching flow entry
        remove          = 0x04, ///< remove matching flow entries
        remove_strict   = 0x05, ///< remove a strictly matching flow entry
};

/// flow entry flags
///
enum class flag : uint16_t
{
        send_flow_removed       = 0x0001, ///< send a flow removed message when a flow is removed
        check_overlap           = 0x0002, ///< check for overlapping entries
        emergency_entry         = 0x0003, ///< use this entry only if the switch is disconnected from the controller
        reset_tracking          = 0x0004, ///< reset packet and byte counts
        no_packet_tracking      = 0x0010, ///< don't count packets
        no_byte_tracking        = 0x0020, ///< don't count bytes
};

} // namespace weftworks::common::openflow::flow
