// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <array>
#include <stdint.h>
#include <string>
#include <type_traits>

namespace weftworks::common::openflow {

/// get the underlying enum type
///
/// \param type is the enum type
template<typename T>
auto constexpr operator +(T type) noexcept -> typename std::underlying_type<T>::type
{
        return static_cast<typename std::underlying_type<T>::type>(type);
}

/// openflow protocol versions
///
enum class version : uint8_t
{
        openflow10      = 0x01,         // openflow protocol v1.0
        openflow11      = 0x02,         // openflow protocol v1.1
        openflow12      = 0x03,         // openflow protocol v1.2
        openflow13      = 0x04,         // openflow protocol v1.3
        openflow14      = 0x05,         // openflow protocol v1.4
        openflow15      = 0x06,         // openflow protocol v1.5
        openflow16      = 0x07,         // openflow protocol v1.6
        latest          = openflow16    // helper value for parsing openflow packets. should always be latest version
};

/// openflow message layer message types
///
enum class message : uint8_t
{
        hello                           = 0x0000,
        error                           = 0x0001,
        echo_request                    = 0x0002,
        echo_response                   = 0x0003,
        experimenter                    = 0x0004,
        get_switch_features_request     = 0x0005,
        get_switch_features_response    = 0x0006,
        get_switch_config_request       = 0x0007,
        get_switch_config_response      = 0x0008,
        set_switch_config               = 0x0009,
        packet_in                       = 0x000a,
        flow_removed                    = 0x000b,
        port_status                     = 0x000c,
        packet_out                      = 0x000d,
        flow_modification               = 0x000e,
        group_modification              = 0x000f,
        port_modification               = 0x0010,
        table_modification              = 0x0011,
        multipart_request               = 0x0012,
        multipart_response              = 0x0013,
        barrier_request                 = 0x0014,
        barrier_response                = 0x0015,
        get_queue_config_request        = 0x0016,
        get_queue_config_response       = 0x0017,
        controller_role_request         = 0x0018,
        controller_role_response        = 0x0019,
        get_async_messages_request      = 0x001a,
        get_async_messages_response     = 0x001b,
        set_async_messages              = 0x001c,
        meter_modification              = 0x001d,
        controller_role_status          = 0x001e,
        table_status                    = 0x001f,
        request_forward                 = 0x0020,
        bundle_control                  = 0x0021,
        bundle_add_message              = 0x0022,
        max_value                       = bundle_add_message,
};

#define DEFAULT_MISS_SEND_LENGTH 128    // default send length in match miss cases
#define ETHERNET_ADDRESS_LENGTH 6       // ethernet address length in bytes
#define MAX_PORT_NAME_LENGTH 16         // port name max length
#define MAX_TABLE_NAME_LENGTH 32        // table name max length

} // namespace weftworks::common::openflow
