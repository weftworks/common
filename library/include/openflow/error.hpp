// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

namespace weftworks::common::openflow::error {

/// high level error type
///
enum class type : uint16_t
{
        hello_failed            = 0x0000, ///< hello message error
        bad_request             = 0x0001, ///< request was not understood
        bad_action              = 0x0002, ///< error in actions
        bad_instruction         = 0x0003, ///< error in instructions
        bad_match               = 0x0004, ///< error in a match
        flow_mod_failed         = 0x0005, ///< error modifying a flow entry
        group_mod_failed        = 0x0006, ///< error modifying a group entry
        port_mod_failed         = 0x0007, ///< port mod request failed
        table_mod_failed        = 0x0008, ///< table mod request failed
        queue_op_failed         = 0x0009, ///< queue operation failed
        switch_config_failed    = 0x000a, ///< switch config request failed
        role_request_failed     = 0x000b, ///< controller role request failed
        meter_mod_failed        = 0x000c, ///< meter mod request failed
        table_features_failed   = 0x000d, ///< setting table features failed
        async_config_failed     = 0x000e, ///< asynchronous config request failed
        flow_monitor_failed     = 0x000f, ///< setting a flow monitor failed
        bundle_failed           = 0x0011, ///< a bundle operation failed
};

/// type specific errors
///
namespace code {

/// asynchronous config request failed
///
enum class async_config_failed : uint16_t
{
        invalid         = 0x0000,
        unsupported     = 0x0001,
        permissions     = 0x0002,
};

/// error in actions
///
enum class bad_action : uint16_t
{
        bad_type                = 0x0000,
        bad_length              = 0x0001,
        bad_experimenter        = 0x0002,
        bad_experimenter_type   = 0x0003,
        bad_out_port            = 0x0004,
        bad_argument            = 0x0005,
        permissions             = 0x0006,
        too_many                = 0x0007,
        bad_queue               = 0x0008,
        bad_out_group           = 0x0009,
        match_inconsistent      = 0x000a,
        unsupported_order       = 0x000b,
        bad_tag                 = 0x000c,
        bad_set_type            = 0x000d,
        bad_set_length          = 0x000e,
        bad_set_argument        = 0x000f,
};

/// error in instructions
///
enum class bad_instruction : uint16_t
{
        unknown_instruction             = 0x0000,
        unsupported_instruction         = 0x0001,
        bad_table_id                    = 0x0002,
        unsupported_metadata            = 0x0003,
        unsupported_metadata_mask       = 0x0004,
        bad_experimenter                = 0x0005,
        bad_experimentertype            = 0x0006,
        bad_length                      = 0x0007,
        permissions                     = 0x0008,
        duplicate_instruction           = 0x0009,
};

/// error in a match
///
enum class bad_match : uint16_t
{
        bad_type                        = 0x0000,
        bad_length                      = 0x0001,
        bad_tag                         = 0x0002,
        bad_datalink_address_mask       = 0x0003,
        bad_hardware_address_mask       = 0x0004,
        bad_wildcards                   = 0x0005,
        bad_field                       = 0x0006,
        bad_value                       = 0x0007,
        bad_mask                        = 0x0008,
        bad_prerequisite                = 0x0009,
        duplicate_field                 = 0x000a,
        permissions                     = 0x000b,
        multipart_reply_timeout         = 0x000f,
};

/// request was not understood
///
enum class bad_request : uint16_t
{
        bad_version                     = 0x0000,
        bad_type                        = 0x0001,
        bad_multipart                   = 0x0002,
        bad_experimenter                = 0x0003,
        bad_experimenter_type           = 0x0004,
        permissions                     = 0x0005,
        bad_length                      = 0x0006,
        buffer_empty                    = 0x0007,
        buffer_unknown                  = 0x0008,
        bad_table_id                    = 0x0009,
        is_slave                        = 0x000a,
        bad_port                        = 0x000b,
        bad_packet                      = 0x000c,
        multipart_buffer_overflow       = 0x000d,
        multipart_request_timeout       = 0x000e,
        multipart_reply_timeout         = 0x000f,
};

/// a bundle operation failed
///
enum class bundle_failed : uint16_t
{
        unknown                 = 0x0000,
        permissions             = 0x0001,
        bad_id                  = 0x0002,
        bundle_exists           = 0x0003,
        bundle_closed           = 0x0004,
        out_of_bundles          = 0x0005,
        bad_type                = 0x0006,
        bad_flags               = 0x0007,
        bad_message_length      = 0x0008,
        bad_message_xid         = 0x0009,
        message_unsupported     = 0x000a,
        message_conflict        = 0x000b,
        too_many                = 0x000c,
        message_failed          = 0x000d,
        timeout                 = 0x000e,
        bundle_in_progress      = 0x000f,
};

/// error modifying a flow entry
///
enum class flow_mod_failed : uint16_t
{
        unknown         = 0x0000,
        table_full      = 0x0001,
        bad_table_id    = 0x0002,
        overlap         = 0x0003,
        permissions     = 0x0004,
        bad_timeout     = 0x0005,
        bad_command     = 0x0006,
        bad_flags       = 0x0007,
        cant_sync       = 0x0008,
        bad_priority    = 0x0009,
};

/// setting a flow monitor failed
///
enum class flow_monitor_failed : uint16_t
{
        unknown                 = 0x0000,
        monitor_exists          = 0x0001,
        invalid_monitor         = 0x0002,
        unknown_monitor         = 0x0003,
        bad_command             = 0x0004,
        bad_flags               = 0x0005,
        bad_table_id            = 0x0006,
        bad_out                 = 0x0007,
};

/// error modifying a group entry
///
enum class group_mod_failed : uint16_t
{
        group_exists            = 0x0000,
        invalid_group           = 0x0001,
        weight_unsupported      = 0x0002,
        out_of_groups           = 0x0003,
        out_of_buckets          = 0x0004,
        chaining_unsupported    = 0x0005,
        watch_unsupported       = 0x0006,
        loop                    = 0x0007,
        unknown_group           = 0x0008,
        chained_group           = 0x0009,
        bad_type                = 0x000a,
        bad_command             = 0x000b,
        bad_bucket              = 0x000c,
        bad_watch               = 0x000d,
        permissions             = 0x000e,
};

/// hello message error
///
enum class hello_failed : uint16_t
{
        incompatible    = 0x0000,
        permissions     = 0x0001,
};

/// meter mod request failed
///
enum class meter_mod_failed : uint16_t
{
        unknown         = 0x0000,
        meter_exists    = 0x0001,
        invalid_meter   = 0x0002,
        unknown_meter   = 0x0003,
        bad_command     = 0x0004,
        bad_flags       = 0x0005,
        bad_rate        = 0x0006,
        bad_burst       = 0x0007,
        bad_band        = 0x0008,
        bad_bandvalue   = 0x0009,
        out_of_meters   = 0x000a,
        out_of_bands    = 0x000b,
};

/// port mod request failed
///
enum class port_mod_failed : uint16_t
{
        bad_port                = 0x0000,
        bad_hardware_address    = 0x0001,
        bad_config              = 0x0002,
        bad_advertise           = 0x0003,
        permissions             = 0x0004,
};

/// queue operation failed
///
enum class queue_op_failed : uint16_t
{
        bad_port        = 0x0000,
        bad_queue       = 0x0001,
        permissions     = 0x0002,
};

/// controller role request failed
///
enum class role_request_failed : uint16_t
{
        stale           = 0x0000,
        unsupported     = 0x0001,
        bad_role        = 0x0002,
};

/// switch config request failed
///
enum class switch_config_failed : uint16_t
{
        bad_flags       = 0x0000,
        bad_length      = 0x0001,
        permissions     = 0x0002,
};

/// setting table features failed
///
enum class table_features_failed : uint16_t
{
        bad_table       = 0x0000,
        bad_metadata    = 0x0001,
        permissions     = 0x0005,
};

/// table mod request failed
///
enum class table_mod_failed : uint16_t
{
        bad_table       = 0x0000,
        bad_config      = 0x0001,
        permissions     = 0x0002,
};

} // namespace code

} // namespace weftworks::common::openflow::error
