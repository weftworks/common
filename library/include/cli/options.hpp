// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iostream>

#include <boost/format.hpp>
#include <boost/program_options.hpp>

namespace weftworks::common::cli {

namespace implementation {

namespace {

using description = boost::program_options::options_description;

}

/// create and store program parameter options
///
class options
{
public:
        /// constructor
        ///
        options();
        /// add a new program option
        ///
        /// \param args are the program option arguments
        /// \param description is the program option description
        template<typename T>
        auto add(const char* args, const char* description) -> void;
        /// check whether an option is set
        ///
        /// \param option is the option name
        /// \return a boolean result
        auto is_set(const std::string& option) -> bool;
        /// get the value of a given option
        ///
        /// \param option is the option name
        /// \return the supplied argument value
        template<typename T>
        auto get_value(const char* option) -> T;
        /// store the given options
        ///
        /// \param argc is the argument count + 1
        /// \param argv is the argument vector
        auto store(int argc, char** argv) -> void;
private:
        boost::program_options::variables_map map;
        description all;
        description general;
        description program;
};

inline options::options()
        : all{description{"available options"}}
        , general{description{"general options"}}
        , program{description{"program options"}}
{
        // define general options.
        general.add_options()
                ("help,h", "print usage message")
                ("version,v", "print version");
}

template<typename T>
auto inline options::add(const char* args, const char* description) -> void
{
        program.add_options()(args, boost::program_options::value<T>(), description);
}

auto inline options::is_set(const std::string& option) -> bool
{
        return map.count(option);
}

template<typename T>
auto inline options::get_value(const char* option) -> T
{
        return map[option].as<T>();
}

auto inline options::store(int argc, char** argv) -> void
{
        // combine general options and program options for storing and printing --help
        all.add(general).add(program);
        // store the options
        try {
                boost::program_options::store(boost::program_options::parse_command_line(argc, argv, all), map);
                boost::program_options::notify(map);
        } catch (std::exception& exception) {
                std::cerr << "Error: " << exception.what() << std::endl;
                std::exit(EXIT_FAILURE);
        } catch (...) {
                std::cerr << "Exception of an unknown type!" << std::endl;
                std::exit(EXIT_FAILURE);
        }
        // print all options and exit by default when help option is used
        if (is_set("help")) {
                std::cout << std::endl << all << std::endl;
                std::exit(EXIT_FAILURE);
        }
}

} // namespace implementation

// lift the implementation
using options = implementation::options;

} // namespace namespace weftworks::common::cli
