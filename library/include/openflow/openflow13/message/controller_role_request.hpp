// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iomanip>
#include <sstream>
#include <string>

#include "openflow/common.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {
namespace implementation {

/// present a controller role request message
///
struct controller_role_request
{
        /// constructor
        ///
        controller_role_request();

        openflow::header header;        ///< openflow header
        uint32_t role;                  ///< the requested role
        uint64_t generation_id;         ///< message validation identifier
};

} // namespace implementation

// lift the implementation
using controller_role_request = implementation::controller_role_request;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const controller_role_request& message) -> std::string;

} // namespace weftworks::common::openflow::openflow13::message
