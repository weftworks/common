// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <string>
#include <string_view>

#include <boost/logic/tribool.hpp>
#include <spdlog/spdlog.h>

#include "openflow/common.hpp"
#include "openflow/hello.hpp"
#include "openflow/header.hpp"

namespace weftworks::common::openflow::openflow13::message {
namespace implementation {

/// extend openflow hello element type with a list of protocol version bitmaps
///
struct hello
{
        /// constructor
        ///
        hello();

        openflow::header header;                                ///< openflow header
        std::vector<openflow::hello::version_bitmap> elements;  ///< sequence of hello elements TODO: fix type
};

// TODO: fix type
auto serialize_hello_element(const openflow::hello::version_bitmap& element) -> std::string;

} // namespace implementation

// lift the implementation
using hello = implementation::hello;

/// serialize payload
///
/// \param message is the openflow message to serialize
auto serialize_payload(const hello& message) -> std::string;

/// parse payload from string
///
/// \param data is the data to parse
auto parse_payload(hello& message, std::string_view data) -> boost::tribool;

} // namespace weftworks::common::openflow::openflow13::message
