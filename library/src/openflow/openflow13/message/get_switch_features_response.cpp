// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "openflow/openflow13/message/get_switch_features_response.hpp"

namespace weftworks::common::openflow::openflow13::message {
namespace implementation {

get_switch_features_response::get_switch_features_response()
        : header{0, 0, 0, 0}
        , datapath_id{0}
        , n_buffers{0}
        , n_tables{0}
        , auxiliary_id{0}
        , padding{0}
        , capabilities{0}
        , reserved{0}
{
        header.version = +openflow::version::openflow13;
        header.type = +openflow::message::get_switch_features_response;
}

} // namespace implementation

auto serialize_payload(const get_switch_features_response& message) -> std::string
{
        auto data = std::string{};

        return data;
}

auto parse_payload(get_switch_features_response& message, std::string_view data) -> boost::tribool
{
        auto constexpr featurereq_length = uint_fast8_t{24};
        auto result = boost::tribool{boost::indeterminate};

        if (data.length() < featurereq_length) {
                return false;
        }

        auto it = data.begin();

        message.datapath_id |= ((uint8_t)*it << 56);    ++it;
        message.datapath_id |= ((uint8_t)*it << 48);    ++it;
        message.datapath_id |= ((uint8_t)*it << 40);    ++it;
        message.datapath_id |= ((uint8_t)*it << 32);    ++it;
        message.datapath_id |= ((uint8_t)*it << 24);    ++it;
        message.datapath_id |= ((uint8_t)*it << 16);    ++it;
        message.datapath_id |= ((uint8_t)*it << 8);     ++it;
        message.datapath_id |= (uint8_t)*it;            ++it;

        message.n_buffers |= ((uint8_t)*it << 24);      ++it;
        message.n_buffers |= ((uint8_t)*it << 16);      ++it;
        message.n_buffers |= ((uint8_t)*it << 8);       ++it;
        message.n_buffers |= (uint8_t)*it;              ++it;

        message.n_tables |= (uint8_t)*it;       ++it;

        message.auxiliary_id |= (uint8_t)*it;   ++it;

        message.padding |= ((uint8_t)*it << 8); ++it;
        message.padding |= (uint8_t)*it;        ++it;

        message.capabilities |= ((uint8_t)*it << 24);   ++it;
        message.capabilities |= ((uint8_t)*it << 16);   ++it;
        message.capabilities |= ((uint8_t)*it << 8);    ++it;
        message.capabilities |= (uint8_t)*it;           ++it;

        message.reserved |= ((uint8_t)*it << 24);       ++it;
        message.reserved |= ((uint8_t)*it << 16);       ++it;
        message.reserved |= ((uint8_t)*it << 8);        ++it;
        message.reserved |= (uint8_t)*it;

        return result;
}

} // namespace weftworks::common::openflow::openflow13::message
