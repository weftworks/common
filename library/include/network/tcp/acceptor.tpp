// Weftworks SDN Solution
// Copyright (C) 2019 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

template<class T, template<class...> typename C>
inline acceptor<T, C>::acceptor(boost::asio::io_context& io_context)
        : tcp_acceptor{io_context}
        , listening_connection{std::nullopt}
{
        logger = spdlog::get("network");
        if (not logger)
                logger = spdlog::default_logger()->clone("network");
}

template<class T, template<class...> typename C>
inline acceptor<T, C>::~acceptor()
{
        if (is_open())
                close();
}

template<class T, template<class...> typename C>
auto inline acceptor<T, C>::get_port() const noexcept -> uint16_t
{
        auto error = boost::system::error_code{};
        auto endpoint = tcp_acceptor.local_endpoint(error);

        return error ? 0 : endpoint.port();
}

template<class T, template<class...> typename C>
auto inline acceptor<T, C>::is_open() const noexcept -> bool
{
        return tcp_acceptor.is_open();
}


template<class T, template<class...> typename C>
auto inline acceptor<T, C>::async_accept(C<T> connection) -> bool
{
        if (not is_able_to_accept(connection))
                return false;

        listening_connection = std::move(connection);

        auto handler = [this](const boost::system::error_code& error)
        {
                handle_accept(error);
        };
        // acceptor class is a friend of the connection class so we can directly access the underlying tcp socket
        tcp_acceptor.async_accept(*listening_connection.value()->socket, handler);
        logger->debug("Listening for a connection..");

        return true;
}


template<class T, template<class...> typename C>
auto inline acceptor<T, C>::accept_one(C<T> connection) -> bool
{
        if (not is_able_to_accept(connection))
                return false;

        listening_connection = std::move(connection);

        // acceptor class is a friend of the connection class so we can directly access the underlying tcp socket
        tcp_acceptor.accept(*listening_connection.value()->socket);
        logger->debug("Listening for a connection..");

        return true;
}

template<class T, template<class...> typename C>
auto inline acceptor<T, C>::bind(std::string_view address, uint16_t port) -> bool
{
        auto error = boost::system::error_code{};

        auto host = boost::asio::ip::make_address(address, error);
        if (error) {
                logger->critical("Failed to resolve address: {}.", error.message());
                return false;
        }

        auto endpoint = boost::asio::ip::tcp::endpoint{host, port};
        tcp_acceptor.open(endpoint.protocol(), error);
        if (error) {
                logger->critical("Failed to open acceptor: {}.", error.message());
                return false;
        }

        // allow rebinding to the same address in case of a quick restart (SO_REUSEADDR)
        tcp_acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));

        tcp_acceptor.bind(endpoint, error);
        if (error) {
                logger->critical("Failed to bind to {}:{}: {}.", address, port, error.message());
                return false;
        }

        auto constexpr max_queue_length = boost::asio::socket_base::max_listen_connections;
        tcp_acceptor.listen(max_queue_length, error);
        if (error) {
                logger->critical("Failed to enter listening state: {}.", error.message());
                return false;
        }

        logger->info("Listening on {}:{}.", address, get_port());
        return true;
}

template<class T, template<class...> typename C>
auto inline acceptor<T, C>::close() -> bool
{
        auto error = boost::system::error_code{};

        tcp_acceptor.cancel(error);
        if (error) {
                logger->error("Failed to stop acceptor: {}.", error.message());
                return false;
        }

        tcp_acceptor.close(error);
        if (error) {
                logger->error("Failed to close acceptor: {}.", error.message());
                return false;
        }

        logger->debug("Acceptor closed.");
        return true;
}

template<class T, template<class...> typename C>
auto inline acceptor<T, C>::handle_accept(const boost::system::error_code& error) -> void
{
        auto connection = std::move(listening_connection.value());

        if (error) {
                logger->error("Failed to accept a connection: %s.", error.message());
                connection = nullptr;
        } else {
                logger->debug("A new connection received.");
        }

        on_accept(std::move(connection));
}

template<class T, template<class...> typename C>
auto inline acceptor<T, C>::on_accept(C<T> connection) -> void
{
        // default behavior
        if (connection)
                connection->start();
}


template<class T, template<class...> typename C>
auto inline acceptor<T, C>::is_able_to_accept(C<T> connection) -> bool
{
        if (not connection) {
                logger->error("Tried to accept on an invalid connection.");
                return false;
        }

        if (listening_connection) {
                logger->error("Already listening.");
                return false;
        }

        return true;
}
