// Weftworks SDN Solution
// Copyright (C) 2018 Antti Lohtaja
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "defensive.hpp"

namespace weftworks::common::utility {

/// revent object copying by disabling the copy constructor and the copy assign operator
///
class noncopyable
{
public:
        /// constructor
        ///
        noncopyable() = default;
        /// destructor
        ///
        ~noncopyable() = default;
        /// move constructor
        ///
        noncopyable(noncopyable&&) = default;
        /// move assignment operator
        ///
        noncopyable& operator=(noncopyable&&) = default;
private:
        /// copy constructor
        ///
        noncopyable(const noncopyable&) = delete;
        /// copy assignment operator
        ///
        noncopyable& operator=(const noncopyable&) = delete;
};

WEFTWORKS_DEFENSIVE_FULFILLS_MOVE_ONLY(noncopyable);

} // namespace weftworks::common::utility
